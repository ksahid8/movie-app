import { RandomMovieItemResponse, MovieDetailResponse } from './types';

const BASE_URL:string = "https://search.imdbot.workers.dev"

export const movieSDK = {
  fetchRandomMovies: async () => {
    try {

      const timestamp = new Date().getTime();
      const randomIndex = timestamp % 26;
      const randomLetter = String.fromCharCode(97 + randomIndex);

      const response = await fetch(`${BASE_URL}/?q=${randomLetter}`);

      if (!response.ok) {
        throw new Error('Failed to fetch random movies');
      }

      const data: RandomMovieItemResponse = await response.json();
      return data.description || [];

    } catch (error) {
      console.error('Error fetching random movies:', error);
      return [];
    }
  },

  searchMovies: async (query: string) => {
    try {
      if (!query) {
        return [];
      }

      const response = await fetch(`${BASE_URL}/?q=${query}`);

      if (!response.ok) {
        throw new Error('Failed to search movies');
      }

      const data:RandomMovieItemResponse = await response.json();
      return data.description || [];

    } catch (error) {
      console.error('Error searching movies:', error);
      return [];
    }
  },

  fetchMovieDetails: async (movieId: string) => {
    try {
      const response = await fetch(`${BASE_URL}/?tt=${movieId}`);

      if (!response.ok) {
        throw new Error('Failed to search movies');
      }

      const data: MovieDetailResponse = await response.json();
      return data;
      
    } catch (error) {
      console.error('Error fetching movie details:', error);
      return null;
    }
  }
};
