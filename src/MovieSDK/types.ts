export type RootStackParamList = {
    Home: undefined;
    Detail: {movieId: string};
    Search: undefined;
  };
  
  export interface RandomMovieItem {
    '#TITLE': string;
    '#YEAR': number;
    '#IMDB_ID': string;
    '#RANK': number;
    '#ACTORS': string;
    '#AKA': string;
    '#IMDB_URL': string;
    '#IMDB_IV': string;
    '#IMG_POSTER': string;
    photo_width: number;
    photo_height: number;
  }
  
  export interface MovieDetailShort {
    '@context'?: string;
    '@type'?: string;
    url?: string;
    name?: string;
    image?: string;
    description?: string;
    review?: {
      '@type'?: string;
      itemReviewed?: {
        '@type'?: string;
        url?: string;
      };
      author?: {
        '@type'?: string;
        name?: string;
      };
      dateCreated?: string;
      inLanguage?: string;
      name?: string;
      reviewBody?: string;
      reviewRating?: {
        '@type'?: string;
        worstRating?: number;
        bestRating?: number;
        ratingValue?: number;
      };
    };
    aggregateRating?: {
      '@type'?: string;
      ratingCount?: number;
      bestRating?: number;
      worstRating?: number;
      ratingValue?: number;
    };
    contentRating?: string;
    genre?: string[];
    datePublished?: string;
    keywords?: string;
    actor?: {
      '@type'?: string;
      url?: string;
      name?: string;
    }[];
    director?: {
      '@type'?: string;
      url?: string;
      name?: string;
    }[];
    creator?: (
      | {
          '@type'?: string;
          url?: string;
        }
      | {
          '@type'?: string;
          url?: string;
          name?: string;
        }
    )[];
    duration?: string;
  }
  
  export interface MovieDetailResponse {
    imdbId?: string;
    short?: MovieDetailShort;
    top?:any;
    main?:any
    fake?:RandomMovieItem
    storyLine?:any
  }
  
  export interface RandomMovieItemResponse {
    description:RandomMovieItem
  }