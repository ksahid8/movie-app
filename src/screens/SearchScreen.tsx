import React, {useCallback, useEffect, useRef, useState} from 'react';
import {FlatList, StyleSheet, TextInput, View} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';
import {useDispatch, useSelector} from 'react-redux';
import {searchMovies} from 'src/redux/actions/movieActions';
import {AppDispatch, RootState} from 'src/redux/reducers/rootReducer';
import SearchListCard from 'src/components/SearchListCard';
import {COLORS} from 'src/utils/colors';
import {RootStackParamList} from 'src/MovieSDK/types';
import SearchListCardSkeleton from 'src/components/SearchListCardSkeleton';

interface SearchScreenProps {
  navigation: StackNavigationProp<RootStackParamList, 'Search'>;
}

const SearchScreen: React.FC<SearchScreenProps> = ({navigation}) => {
  const [searchQuery, setSearchQuery] = useState('');
  const searchInputRef = useRef<TextInput>(null);
  const dispatch: AppDispatch = useDispatch();
  const [loading, setLoading] = useState(false); // Changed initial state to false

  const searchResults = useSelector(
    (state: RootState) => state.movies.searchResults,
  );

  const handleSearch = useCallback(
    (query: string) => {
      setSearchQuery(query);
      setLoading(true);
      dispatch(searchMovies(query))
        .then(() => setLoading(false))
        .catch(() => setLoading(false));
    },
    [dispatch],
  );

  useEffect(() => {
    if (searchInputRef.current) {
      searchInputRef.current.focus();
    }

    return () => {
      setSearchQuery('');
      dispatch(searchMovies(''));
    };
  }, []);

  return (
    <View style={styles.container}>
      <TextInput
        ref={searchInputRef}
        style={styles.searchInput}
        placeholder="Type movie name.."
        onChangeText={handleSearch}
        value={searchQuery}
      />
       <FlatList
          data={searchResults.slice(0, 10)}
          renderItem={({item}) =>
            loading ? (
              <SearchListCardSkeleton />
            ) : (
              <SearchListCard
                onPress={() =>
                  navigation.navigate('Detail', {movieId: item['#IMDB_ID']})
                }
                item={item}
              />
            )
          }
          keyExtractor={item => item['#IMDB_ID'].toString()}
          contentContainerStyle={styles.suggestionList}
          initialNumToRender={5}
          maxToRenderPerBatch={10}
          windowSize={10}
        />
    </View>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.whiteBg,
    paddingHorizontal: 10,
  },
  searchInput: {
    height: 40,
    borderWidth: 1,
    borderColor: COLORS.grey,
    borderRadius: 5,
    paddingHorizontal: 10,
    marginVertical: 10,
  },
  suggestionList: {
    marginTop: 10,
  },
  emptyContainer: {
    alignItems: 'center',
  },
  emptyText: {
    fontSize: 14,
    color: COLORS.blackText,
  },
});

export default SearchScreen;
