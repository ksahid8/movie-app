import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {useSelector} from 'react-redux';
import {useDispatch} from 'react-redux';
import Cast from 'src/components/detailScreen/Cast';
import DetailSkeleton from 'src/components/detailScreen/DetailSkeleton';
import Header from 'src/components/detailScreen/Header';
import Keywords from 'src/components/detailScreen/Keywords';
import Overview from 'src/components/detailScreen/Overview';
import Review from 'src/components/detailScreen/Review';
import {fetchMovieDetails} from 'src/redux/actions/movieActions';
import {AppDispatch, RootState} from 'src/redux/reducers/rootReducer';
import {COLORS} from 'src/utils/colors';
import {MovieDetailResponse, RootStackParamList} from 'src/MovieSDK/types';

type DetailScreenRouteProp = RouteProp<RootStackParamList, 'Detail'>;

type DetailScreenProps = {
  navigation: StackNavigationProp<RootStackParamList, 'Home'>;
  route: DetailScreenRouteProp;
};

const DetailScreen: React.FC<DetailScreenProps> = ({route, navigation}) => {
  const {movieId} = route.params;
  const [loading, setLoading] = useState(true);

  const dispatch: AppDispatch = useDispatch();
  const movieDetails: MovieDetailResponse = useSelector(
    (state: RootState) => state.movies.movieDetails,
  );

  useEffect(() => {
    dispatch(fetchMovieDetails(movieId))
      .then(() => setLoading(false))
      .catch(() => setLoading(false));
  }, [dispatch, movieId]);

  return (
    <View style={styles.detailScreen}>
      <ScrollView>
        {loading ? (
          <DetailSkeleton />
        ) : (
          <View>
            <Header movieDetails={movieDetails} />
            <Overview movieDetails={movieDetails} />
            <Cast movieDetails={movieDetails} />
            <Keywords movieDetails={movieDetails} />
            <Review movieDetails={movieDetails} />
          </View>
        )}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  detailScreen: {
    flex: 1,
    backgroundColor: COLORS.whiteBg,
  },
});

export default DetailScreen;
