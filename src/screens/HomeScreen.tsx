import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';
import MovieCard from 'src/components/MovieCard';
import {RootStackParamList} from 'src/MovieSDK/types';
import SearchBar from 'src/components/SearchBar';
import {COLORS} from 'src/utils/colors';
import {AppDispatch, RootState} from 'src/redux/reducers/rootReducer';
import {useDispatch} from 'react-redux';
import {useSelector} from 'react-redux';
import {fetchRandomMovies} from 'src/redux/actions/movieActions';
import MovieCardSkeleton from 'src/components/MovieCardSkeleton';

type HomeScreenProps = {
  navigation: StackNavigationProp<RootStackParamList, 'Home'>;
};

const HomeScreen: React.FC<HomeScreenProps> = ({navigation}) => {
  const [loading, setLoading] = useState(true);

  const dispatch: AppDispatch = useDispatch();
  const randomMovies = useSelector(
    (state: RootState) => state.movies.randomMovies,
  );

  const fetchMovies = () => {
    setLoading(true);
    dispatch(fetchRandomMovies())
      .then(() => setLoading(false))
      .catch(() => setLoading(false));
  };

  useEffect(() => {
    fetchMovies();
  }, []);

  return (
    <View>
      <SearchBar onPress={() => navigation.navigate('Search')} />
      <View>
        <FlatList
          refreshing={loading}
          onRefresh={fetchMovies}
          data={randomMovies.slice(0, 10)}
          renderItem={({item}) =>
            loading ? (
              <MovieCardSkeleton />
            ) : (
              <MovieCard
                movie={item}
                onPress={() =>
                  navigation.navigate('Detail', {movieId: item['#IMDB_ID']})
                }
              />
            )
          }
          keyExtractor={movie => movie['#IMDB_ID'].toString()}
          contentContainerStyle={styles.listContainer}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.whiteBg,
  },
  listContainer: {
    paddingBottom: 150,
    backgroundColor: COLORS.lightBg,
    paddingHorizontal: 10,
  },
});

export default HomeScreen;
