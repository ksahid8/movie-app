export const COLORS = {
    whiteBg:"#fff",
    whiteText:"#fff",
    blackBg:"#222",
    blackText:"#222",
    lightBg:"#f3f5f7",
    grey:"#ccc",
    greyText:'#787878',
    blue:"#136CB2",
    yellow:"#f5c518",
    red:"red"
}