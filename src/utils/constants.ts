import {Dimensions} from 'react-native';

export const DETAILS_HEIGHT_SKELETON =
  (Dimensions.get('screen').height * 0.6) / 1.5;

export const formatVoteCount = (count: number | undefined) => {
  if (!count) {
    return 0;
  }
  if (count >= 1000) {
    return (count / 1000).toFixed(1) + 'k';
  }
  return count.toString();
};

export const formatDate = (inputDate: string) => {
  const months = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];

  const parsedDate = new Date(inputDate);

  const day = parsedDate.getDate();
  const month = months[parsedDate.getMonth()];
  const year = parsedDate.getFullYear();
  const formattedDate = `${day} ${month} ${year}`;

  return formattedDate;
};
