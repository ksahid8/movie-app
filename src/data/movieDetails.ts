import { MovieDetailResponse } from "src/MovieSDK/types"

const MOVIE_DETAILS:MovieDetailResponse = {
  short: {
    '@context': 'https://schema.org',
    '@type': 'Movie',
    url: 'https://www.imdb.com/title/tt0320736/',
    name: 'Vikram',
    image:
      'https://m.media-amazon.com/images/M/MV5BOWI0MjY4MjItOWYzZC00Y2I0LThiNjYtMWU1NjFhNWQxNDhkXkEyXkFqcGdeQXVyOTIzODUxMjk@._V1_.jpg',
    description:
      'Vikram, a cop who is mourning the death of his wife, is assigned to retrieve a missile. He is aided by a highly educated engineer Preethi, who holds the knowledge to disarm the nuke, in his mission.',
    review: {
      '@type': 'Review',
      itemReviewed: {
        '@type': 'Movie',
        url: 'https://www.imdb.com/title/tt0320736/',
      },
      author: {'@type': 'Person', name: 'sanjayppc'},
      dateCreated: '2014-09-17',
      inLanguage: 'English',
      name: 'Sci-fi gone astray',
      reviewBody:
        'Vikram was one of the ambitious film projects of kamal, who collaborated with writer Sujatha. This was the last film in which Satyaraj acted as villain.\n\nHe had started acting as hero then &amp; was reluctant. But kamal insisted that he act as the villain with the assurance that he would later produce a film with him as hero. That film was &quot;Kadamai Kanniyam kattupadu&quot;.\n\nVikram had a good cast, lovely locales, melodious songs, gorgeous heroines &amp; a smart &amp; handsome kamal as a cop, yet it failed at the box office.\n\nThe reason was that that the usual masala elements were missing. Also, after a riveting first half, the film went haywire in the 2nd half when the story moved to Salamia (imaginary country). Janakaraj&apos;s antics were annoying &amp; also the pace of the film slackened a bit.\n\nThe trailer was in the theatres for a few months before its release raising great expectations, specially amongst kamal fans.\n\nOver the years, it has gone on to become a classic &amp; shown repeatedly on TV.',
      reviewRating: {
        '@type': 'Rating',
        worstRating: 1,
        bestRating: 10,
        ratingValue: 7,
      },
    },
    aggregateRating: {
      '@type': 'AggregateRating',
      ratingCount: 1137,
      bestRating: 10,
      worstRating: 1,
      ratingValue: 7.3,
    },
    contentRating: 'U',
    genre: ['Action', 'Adventure', 'Sci-Fi'],
    datePublished: '1986-05-29',
    keywords:
      'murder of a pregnant woman,murder,pregnancy,one word title,princess',
    actor: [
      {
        '@type': 'Person',
        url: 'https://www.imdb.com/name/nm0352032/',
        name: 'Kamal Haasan',
      },
      {
        '@type': 'Person',
        url: 'https://www.imdb.com/name/nm0766470/',
        name: 'Sathyaraj',
      },
      {
        '@type': 'Person',
        url: 'https://www.imdb.com/name/nm0024302/',
        name: 'Ambika',
      },
    ],
    director: [
      {
        '@type': 'Person',
        url: 'https://www.imdb.com/name/nm2158003/',
        name: 'Rajasekar',
      },
    ],
    creator: [
      {'@type': 'Organization', url: 'https://www.imdb.com/company/co0026065/'},
      {
        '@type': 'Person',
        url: 'https://www.imdb.com/name/nm0352032/',
        name: 'Kamal Haasan',
      },
      {
        '@type': 'Person',
        url: 'https://www.imdb.com/name/nm0837675/',
        name: 'Sujatha',
      },
    ],
    duration: 'PT2H16M',
  },
  imdbId: 'tt0320736',
  top: {
    id: 'tt0320736',
    productionStatus: {
      currentProductionStage: {
        id: 'released',
        text: 'Released',
        __typename: 'ProductionStage',
      },
      productionStatusHistory: [
        {
          status: {
            id: 'released',
            text: 'Released',
            __typename: 'ProductionStatus',
          },
          __typename: 'ProductionStatusHistory',
        },
      ],
      restriction: null,
      __typename: 'ProductionStatusDetails',
    },
    canHaveEpisodes: false,
    series: null,
    titleText: {text: 'Vikram', __typename: 'TitleText'},
    titleType: {
      displayableProperty: {
        value: {plainText: '', __typename: 'Markdown'},
        __typename: 'DisplayableTitleTypeProperty',
      },
      text: 'Movie',
      id: 'movie',
      isSeries: false,
      isEpisode: false,
      categories: [{value: 'movie', __typename: 'TitleTypeCategory'}],
      canHaveEpisodes: false,
      __typename: 'TitleType',
    },
    originalTitleText: {text: 'Vikram', __typename: 'TitleText'},
    certificate: {rating: 'U', __typename: 'Certificate'},
    releaseYear: {year: 1986, endYear: null, __typename: 'YearRange'},
    releaseDate: {day: 29, month: 5, year: 1986, __typename: 'ReleaseDate'},
    runtime: {
      seconds: 8160,
      displayableProperty: {
        value: {plainText: '2h 16m', __typename: 'Markdown'},
        __typename: 'DisplayableTitleRuntimeProperty',
      },
      __typename: 'Runtime',
    },
    canRate: {isRatable: true, __typename: 'CanRate'},
    ratingsSummary: {
      aggregateRating: 7.3,
      voteCount: 1137,
      __typename: 'RatingsSummary',
    },
    meterRanking: null,
    primaryImage: {
      id: 'rm3975111424',
      width: 1599,
      height: 2048,
      url: 'https://m.media-amazon.com/images/M/MV5BOWI0MjY4MjItOWYzZC00Y2I0LThiNjYtMWU1NjFhNWQxNDhkXkEyXkFqcGdeQXVyOTIzODUxMjk@._V1_.jpg',
      caption: {
        plainText: 'Kamal Haasan in Vikram (1986)',
        __typename: 'Markdown',
      },
      __typename: 'Image',
    },
    images: {
      total: 58,
      edges: [
        {
          node: {id: 'rm2872056833', __typename: 'Image'},
          __typename: 'ImageEdge',
        },
      ],
      __typename: 'ImageConnection',
    },
    videos: {total: 0, __typename: 'TitleRelatedVideosConnection'},
    primaryVideos: {edges: [], __typename: 'VideoConnection'},
    externalLinks: {total: 2, __typename: 'ExternalLinkConnection'},
    metacritic: null,
    keywords: {
      total: 17,
      edges: [
        {
          node: {
            text: 'murder of a pregnant woman',
            __typename: 'TitleKeyword',
          },
          __typename: 'TitleKeywordEdge',
        },
        {
          node: {text: 'murder', __typename: 'TitleKeyword'},
          __typename: 'TitleKeywordEdge',
        },
        {
          node: {text: 'pregnancy', __typename: 'TitleKeyword'},
          __typename: 'TitleKeywordEdge',
        },
        {
          node: {text: 'one word title', __typename: 'TitleKeyword'},
          __typename: 'TitleKeywordEdge',
        },
        {
          node: {text: 'princess', __typename: 'TitleKeyword'},
          __typename: 'TitleKeywordEdge',
        },
      ],
      __typename: 'TitleKeywordConnection',
    },
    genres: {
      genres: [
        {text: 'Action', id: 'Action', __typename: 'Genre'},
        {text: 'Adventure', id: 'Adventure', __typename: 'Genre'},
        {text: 'Sci-Fi', id: 'Sci-Fi', __typename: 'Genre'},
      ],
      __typename: 'Genres',
    },
    plot: {
      plotText: {
        plainText:
          'Vikram, a cop who is mourning the death of his wife, is assigned to retrieve a missile. He is aided by a highly educated engineer Preethi, who holds the knowledge to disarm the nuke, in his mission.',
        __typename: 'Markdown',
      },
      language: {id: 'en-US', __typename: 'DisplayableLanguage'},
      __typename: 'Plot',
    },
    plotContributionLink: {
      url: 'https://contribute.imdb.com/updates?update=tt0320736:outlines.add.1.locale~en-US',
      __typename: 'ContributionLink',
    },
    credits: {total: 33, __typename: 'CreditConnection'},
    principalCredits: [
      {
        totalCredits: 1,
        category: {
          text: 'Director',
          id: 'director',
          __typename: 'CreditCategory',
        },
        credits: [
          {
            name: {
              nameText: {text: 'Rajasekar', __typename: 'NameText'},
              id: 'nm2158003',
              __typename: 'Name',
            },
            attributes: null,
            __typename: 'Crew',
          },
        ],
        __typename: 'PrincipalCreditsForCategory',
      },
      {
        totalCredits: 2,
        category: {text: 'Writers', id: 'writer', __typename: 'CreditCategory'},
        credits: [
          {
            name: {
              nameText: {text: 'Kamal Haasan', __typename: 'NameText'},
              id: 'nm0352032',
              __typename: 'Name',
            },
            attributes: null,
            __typename: 'Crew',
          },
          {
            name: {
              nameText: {text: 'Sujatha', __typename: 'NameText'},
              id: 'nm0837675',
              __typename: 'Name',
            },
            attributes: null,
            __typename: 'Crew',
          },
        ],
        __typename: 'PrincipalCreditsForCategory',
      },
      {
        totalCredits: 16,
        category: {text: 'Stars', id: 'cast', __typename: 'CreditCategory'},
        credits: [
          {
            name: {
              nameText: {text: 'Kamal Haasan', __typename: 'NameText'},
              id: 'nm0352032',
              __typename: 'Name',
            },
            attributes: null,
            __typename: 'Cast',
          },
          {
            name: {
              nameText: {text: 'Sathyaraj', __typename: 'NameText'},
              id: 'nm0766470',
              __typename: 'Name',
            },
            attributes: null,
            __typename: 'Cast',
          },
          {
            name: {
              nameText: {text: 'Ambika', __typename: 'NameText'},
              id: 'nm0024302',
              __typename: 'Name',
            },
            attributes: null,
            __typename: 'Cast',
          },
        ],
        __typename: 'PrincipalCreditsForCategory',
      },
    ],
    reviews: {total: 9, __typename: 'ReviewsConnection'},
    criticReviewsTotal: {total: 1, __typename: 'ExternalLinkConnection'},
    triviaTotal: {total: 3, __typename: 'TriviaConnection'},
    engagementStatistics: {
      watchlistStatistics: {
        displayableCount: {
          text: 'Added by 1.4K users',
          __typename: 'LocalizedDisplayableCount',
        },
        __typename: 'WatchlistStatistics',
      },
      __typename: 'EngagementStatistics',
    },
    subNavCredits: {total: 33, __typename: 'CreditConnection'},
    subNavReviews: {total: 9, __typename: 'ReviewsConnection'},
    subNavTrivia: {total: 3, __typename: 'TriviaConnection'},
    subNavFaqs: {total: 0, __typename: 'FaqConnection'},
    subNavTopQuestions: {total: 14, __typename: 'AlexaQuestionConnection'},
    titleGenres: {
      genres: [
        {
          genre: {text: 'Action', __typename: 'GenreItem'},
          __typename: 'TitleGenre',
        },
        {
          genre: {text: 'Adventure', __typename: 'GenreItem'},
          __typename: 'TitleGenre',
        },
        {
          genre: {text: 'Sci-Fi', __typename: 'GenreItem'},
          __typename: 'TitleGenre',
        },
      ],
      __typename: 'TitleGenres',
    },
    meta: {
      canonicalId: 'tt0320736',
      publicationStatus: 'PUBLISHED',
      __typename: 'TitleMeta',
    },
    castPageTitle: {
      edges: [
        {
          node: {
            name: {
              id: 'nm0352032',
              nameText: {text: 'Kamal Haasan', __typename: 'NameText'},
              __typename: 'Name',
            },
            __typename: 'Cast',
          },
          __typename: 'CreditEdge',
        },
        {
          node: {
            name: {
              id: 'nm0766470',
              nameText: {text: 'Sathyaraj', __typename: 'NameText'},
              __typename: 'Name',
            },
            __typename: 'Cast',
          },
          __typename: 'CreditEdge',
        },
        {
          node: {
            name: {
              id: 'nm0024302',
              nameText: {text: 'Ambika', __typename: 'NameText'},
              __typename: 'Name',
            },
            __typename: 'Cast',
          },
          __typename: 'CreditEdge',
        },
        {
          node: {
            name: {
              id: 'nm0080248',
              nameText: {text: 'Santhana Bharathi', __typename: 'NameText'},
              __typename: 'Name',
            },
            __typename: 'Cast',
          },
          __typename: 'CreditEdge',
        },
      ],
      __typename: 'CreditConnection',
    },
    creatorsPageTitle: [],
    directorsPageTitle: [
      {
        credits: [
          {
            name: {
              id: 'nm2158003',
              nameText: {text: 'Rajasekar', __typename: 'NameText'},
              __typename: 'Name',
            },
            __typename: 'Crew',
          },
        ],
        __typename: 'PrincipalCreditsForCategory',
      },
    ],
    countriesOfOrigin: {
      countries: [{id: 'IN', __typename: 'CountryOfOrigin'}],
      __typename: 'CountriesOfOrigin',
    },
    production: {
      edges: [
        {
          node: {
            company: {
              id: 'co0026065',
              companyText: {
                text: 'Raajkamal Films International',
                __typename: 'CompanyText',
              },
              __typename: 'Company',
            },
            __typename: 'CompanyCredit',
          },
          __typename: 'CompanyCreditEdge',
        },
      ],
      __typename: 'CompanyCreditConnection',
    },
    featuredReviews: {
      edges: [
        {
          node: {
            author: {nickName: 'sanjayppc', __typename: 'UserProfile'},
            summary: {
              originalText: 'Sci-fi gone astray',
              __typename: 'ReviewSummary',
            },
            text: {
              originalText: {
                plainText:
                  'Vikram was one of the ambitious film projects of kamal, who collaborated with writer Sujatha. This was the last film in which Satyaraj acted as villain.\n\nHe had started acting as hero then & was reluctant. But kamal insisted that he act as the villain with the assurance that he would later produce a film with him as hero. That film was "Kadamai Kanniyam kattupadu".\n\nVikram had a good cast, lovely locales, melodious songs, gorgeous heroines & a smart & handsome kamal as a cop, yet it failed at the box office.\n\nThe reason was that that the usual masala elements were missing. Also, after a riveting first half, the film went haywire in the 2nd half when the story moved to Salamia (imaginary country). Janakaraj\'s antics were annoying & also the pace of the film slackened a bit.\n\nThe trailer was in the theatres for a few months before its release raising great expectations, specially amongst kamal fans.\n\nOver the years, it has gone on to become a classic & shown repeatedly on TV.',
                __typename: 'Markdown',
              },
              __typename: 'ReviewText',
            },
            authorRating: 7,
            submissionDate: '2014-09-17',
            __typename: 'Review',
          },
          __typename: 'ReviewEdge',
        },
      ],
      __typename: 'ReviewsConnection',
    },
    __typename: 'Title',
  },
  main: {
    id: 'tt0320736',
    wins: {total: 0, __typename: 'AwardNominationConnection'},
    nominations: {total: 0, __typename: 'AwardNominationConnection'},
    prestigiousAwardSummary: null,
    ratingsSummary: {topRanking: null, __typename: 'RatingsSummary'},
    episodes: null,
    videos: {total: 0, __typename: 'TitleRelatedVideosConnection'},
    videoStrip: {edges: [], __typename: 'VideoConnection'},
    titleMainImages: {
      total: 58,
      edges: [
        {
          node: {
            id: 'rm2872056833',
            url: 'https://m.media-amazon.com/images/M/MV5BYTNlMDE1NzYtZWZiNC00ODJmLWE2MDgtNDE0NTVlYWUyNjRkXkEyXkFqcGdeQXVyOTIzODUxMjk@._V1_.jpg',
            caption: {
              plainText: 'Kamal Haasan and Lizy in Vikram (1986)',
              __typename: 'Markdown',
            },
            height: 1397,
            width: 990,
            __typename: 'Image',
          },
          __typename: 'ImageEdge',
        },
        {
          node: {
            id: 'rm1466244097',
            url: 'https://m.media-amazon.com/images/M/MV5BNDRkZDkxMjItMjgxYS00OWVhLTljY2QtMGRiMWY1ZjQxZGQ2XkEyXkFqcGdeQXVyOTIzODUxMjk@._V1_.jpg',
            caption: {plainText: 'Vikram (1986)', __typename: 'Markdown'},
            height: 947,
            width: 1395,
            __typename: 'Image',
          },
          __typename: 'ImageEdge',
        },
        {
          node: {
            id: 'rm1248140289',
            url: 'https://m.media-amazon.com/images/M/MV5BYjY3ZjM3NGEtMzg5Zi00NDE4LWE2MjgtZjkwYTA1YTkxOTgyXkEyXkFqcGdeQXVyOTIzODUxMjk@._V1_.jpg',
            caption: {plainText: 'Vikram (1986)', __typename: 'Markdown'},
            height: 1452,
            width: 2048,
            __typename: 'Image',
          },
          __typename: 'ImageEdge',
        },
        {
          node: {
            id: 'rm1315249153',
            url: 'https://m.media-amazon.com/images/M/MV5BYjA4YWQ1ZDctNjQ1ZC00MWM0LWEzZmItZTIzYjQwZmZjNGQ5XkEyXkFqcGdeQXVyOTIzODUxMjk@._V1_.jpg',
            caption: {plainText: 'Vikram (1986)', __typename: 'Markdown'},
            height: 1342,
            width: 2048,
            __typename: 'Image',
          },
          __typename: 'ImageEdge',
        },
        {
          node: {
            id: 'rm3202659585',
            url: 'https://m.media-amazon.com/images/M/MV5BODMyMjJjMzgtZTI1Yy00NmUxLTg5M2ItMTMzMjUxNDE1Yzk3XkEyXkFqcGdeQXVyODEzOTQwNTY@._V1_.jpg',
            caption: {
              plainText: 'Kamal Haasan in Vikram (1986)',
              __typename: 'Markdown',
            },
            height: 1221,
            width: 2048,
            __typename: 'Image',
          },
          __typename: 'ImageEdge',
        },
        {
          node: {
            id: 'rm1798144257',
            url: 'https://m.media-amazon.com/images/M/MV5BOGEyYWRkOGEtZDhiMy00NTRkLWE5ODctYWNjYzdlMWUxNDEyXkEyXkFqcGdeQXVyODEzOTQwNTY@._V1_.jpg',
            caption: {
              plainText: 'Kamal Haasan and Dimple Kapadia in Vikram (1986)',
              __typename: 'Markdown',
            },
            height: 1156,
            width: 1600,
            __typename: 'Image',
          },
          __typename: 'ImageEdge',
        },
        {
          node: {
            id: 'rm3337577216',
            url: 'https://m.media-amazon.com/images/M/MV5BNTI1Y2E2YzQtNjA5MS00OGViLWFmMmEtODY5ZjgzYWE5NGExXkEyXkFqcGdeQXVyOTIzODUxMjk@._V1_.jpg',
            caption: {
              plainText: 'Kamal Haasan in Vikram (1986)',
              __typename: 'Markdown',
            },
            height: 2048,
            width: 1400,
            __typename: 'Image',
          },
          __typename: 'ImageEdge',
        },
        {
          node: {
            id: 'rm4176438016',
            url: 'https://m.media-amazon.com/images/M/MV5BZDJlMGQzMmItYjg5ZS00Zjg4LWI5NGQtMmYzNjY5ZTdlYWMzXkEyXkFqcGdeQXVyOTIzODUxMjk@._V1_.jpg',
            caption: {
              plainText: 'Kamal Haasan in Vikram (1986)',
              __typename: 'Markdown',
            },
            height: 2048,
            width: 1429,
            __typename: 'Image',
          },
          __typename: 'ImageEdge',
        },
        {
          node: {
            id: 'rm4209992448',
            url: 'https://m.media-amazon.com/images/M/MV5BMjU0M2YwYWQtNjg1Yi00ZWFhLWEwNjEtZTY3OTZlZjgxNGM2XkEyXkFqcGdeQXVyOTIzODUxMjk@._V1_.jpg',
            caption: {
              plainText: 'Kamal Haasan in Vikram (1986)',
              __typename: 'Markdown',
            },
            height: 1569,
            width: 2048,
            __typename: 'Image',
          },
          __typename: 'ImageEdge',
        },
        {
          node: {
            id: 'rm4226769664',
            url: 'https://m.media-amazon.com/images/M/MV5BODkwY2MxNzgtZGUxZS00NTYxLTlkN2MtZDZjNWVhMmEyM2FkXkEyXkFqcGdeQXVyOTIzODUxMjk@._V1_.jpg',
            caption: {
              plainText: 'Kamal Haasan in Vikram (1986)',
              __typename: 'Markdown',
            },
            height: 2048,
            width: 1344,
            __typename: 'Image',
          },
          __typename: 'ImageEdge',
        },
        {
          node: {
            id: 'rm4243546880',
            url: 'https://m.media-amazon.com/images/M/MV5BYmFmNjc2ODAtZjMyNC00N2Y3LWE1YWYtNjNhNGM3ZTQxMDY3XkEyXkFqcGdeQXVyOTIzODUxMjk@._V1_.jpg',
            caption: {
              plainText: 'Kamal Haasan and Lizy in Vikram (1986)',
              __typename: 'Markdown',
            },
            height: 614,
            width: 630,
            __typename: 'Image',
          },
          __typename: 'ImageEdge',
        },
        {
          node: {
            id: 'rm4277101312',
            url: 'https://m.media-amazon.com/images/M/MV5BOWY3M2ZkOGYtYjkwMi00OWEzLTk4ZjktYTBjY2ZhM2FjZDMyXkEyXkFqcGdeQXVyOTIzODUxMjk@._V1_.jpg',
            caption: {
              plainText:
                'Kamal Haasan, Janakaraj, Dimple Kapadia, Amjad Khan, and Aachi Manorama in Vikram (1986)',
              __typename: 'Markdown',
            },
            height: 607,
            width: 1002,
            __typename: 'Image',
          },
          __typename: 'ImageEdge',
        },
        {
          node: {
            id: 'rm4075774720',
            url: 'https://m.media-amazon.com/images/M/MV5BYzg4ZmMzYmUtNjhkZC00ODk3LThmYzYtODg2MDVlODNjZDFjXkEyXkFqcGdeQXVyOTIzODUxMjk@._V1_.jpg',
            caption: {
              plainText: 'Ambika and Kamal Haasan in Vikram (1986)',
              __typename: 'Markdown',
            },
            height: 1017,
            width: 929,
            __typename: 'Image',
          },
          __typename: 'ImageEdge',
        },
      ],
      __typename: 'ImageConnection',
    },
    productionStatus: {
      currentProductionStage: {
        id: 'released',
        text: 'Released',
        __typename: 'ProductionStage',
      },
      productionStatusHistory: [
        {
          status: {
            id: 'released',
            text: 'Released',
            __typename: 'ProductionStatus',
          },
          __typename: 'ProductionStatusHistory',
        },
      ],
      restriction: null,
      __typename: 'ProductionStatusDetails',
    },
    primaryImage: {id: 'rm3975111424', __typename: 'Image'},
    imageUploadLink: {
      url: 'https://contribute.imdb.com/image/tt0320736/add?bus=imdb&return_url=https%3A%2F%2Fwww.imdb.com%2Fclose_me&site=web',
      __typename: 'ContributionLink',
    },
    titleType: {id: 'movie', canHaveEpisodes: false, __typename: 'TitleType'},
    cast: {
      edges: [
        {
          node: {
            name: {
              id: 'nm0352032',
              nameText: {text: 'Kamal Haasan', __typename: 'NameText'},
              primaryImage: {
                url: 'https://m.media-amazon.com/images/M/MV5BYmUxNTY0MWItODQ2My00YWMyLWFmODgtNTY1MTQ2ZjEwYzdlXkEyXkFqcGdeQXVyMjYwMDk5NjE@._V1_.jpg',
                width: 833,
                height: 1050,
                __typename: 'Image',
              },
              __typename: 'Name',
            },
            attributes: null,
            category: {id: 'actor', __typename: 'CreditCategory'},
            characters: [{name: 'Arun Kumar Vikram', __typename: 'Character'}],
            episodeCredits: {
              total: 0,
              yearRange: null,
              __typename: 'EpisodeCastConnection',
            },
            __typename: 'Cast',
          },
          __typename: 'CreditEdge',
        },
        {
          node: {
            name: {
              id: 'nm0766470',
              nameText: {text: 'Sathyaraj', __typename: 'NameText'},
              primaryImage: {
                url: 'https://m.media-amazon.com/images/M/MV5BMmE2MzUxYTktNTc3My00NWVkLWJiNzMtNjk4ZGEwMWFiZjUwXkEyXkFqcGdeQXVyMjYwMDk5NjE@._V1_.jpg',
                width: 980,
                height: 1491,
                __typename: 'Image',
              },
              __typename: 'Name',
            },
            attributes: null,
            category: {id: 'actor', __typename: 'CreditCategory'},
            characters: [{name: 'Sugurthuraj', __typename: 'Character'}],
            episodeCredits: {
              total: 0,
              yearRange: null,
              __typename: 'EpisodeCastConnection',
            },
            __typename: 'Cast',
          },
          __typename: 'CreditEdge',
        },
        {
          node: {
            name: {
              id: 'nm0024302',
              nameText: {text: 'Ambika', __typename: 'NameText'},
              primaryImage: {
                url: 'https://m.media-amazon.com/images/M/MV5BNzUzMjFhZGQtMzc3My00MTZhLWIwMDAtMWJmY2JjMWFkOTVlXkEyXkFqcGdeQXVyMjYwMDk5NjE@._V1_.jpg',
                width: 637,
                height: 1065,
                __typename: 'Image',
              },
              __typename: 'Name',
            },
            attributes: null,
            category: {id: 'actress', __typename: 'CreditCategory'},
            characters: [{name: 'Vikram wife', __typename: 'Character'}],
            episodeCredits: {
              total: 0,
              yearRange: null,
              __typename: 'EpisodeCastConnection',
            },
            __typename: 'Cast',
          },
          __typename: 'CreditEdge',
        },
        {
          node: {
            name: {
              id: 'nm0080248',
              nameText: {text: 'Santhana Bharathi', __typename: 'NameText'},
              primaryImage: {
                url: 'https://m.media-amazon.com/images/M/MV5BNTA3YWM5YmQtMTU1OS00ZjcwLTlmMTEtNzYwZTM1YzllMTY1XkEyXkFqcGdeQXVyMzQ0NTk5NzU@._V1_.jpg',
                width: 1098,
                height: 735,
                __typename: 'Image',
              },
              __typename: 'Name',
            },
            attributes: null,
            category: {id: 'actor', __typename: 'CreditCategory'},
            characters: [{name: 'ThankaRaj', __typename: 'Character'}],
            episodeCredits: {
              total: 0,
              yearRange: null,
              __typename: 'EpisodeCastConnection',
            },
            __typename: 'Cast',
          },
          __typename: 'CreditEdge',
        },
        {
          node: {
            name: {
              id: 'nm1232849',
              nameText: {text: 'Chinnijayanth', __typename: 'NameText'},
              primaryImage: {
                url: 'https://m.media-amazon.com/images/M/MV5BZTZiNTU5YzktODA2My00MWJjLWI4NTgtZDVkNzU3NWFlN2M3XkEyXkFqcGdeQXVyMzYxOTQ3MDg@._V1_.jpg',
                width: 1200,
                height: 1500,
                __typename: 'Image',
              },
              __typename: 'Name',
            },
            attributes: [
              {
                text: 'as Chinni Jayanth',
                __typename: 'CreditedAsCreditAttribute',
              },
            ],
            category: {id: 'actor', __typename: 'CreditCategory'},
            characters: [{name: 'Local boy', __typename: 'Character'}],
            episodeCredits: {
              total: 0,
              yearRange: null,
              __typename: 'EpisodeCastConnection',
            },
            __typename: 'Cast',
          },
          __typename: 'CreditEdge',
        },
        {
          node: {
            name: {
              id: 'nm0223502',
              nameText: {text: 'Vikram Dharma', __typename: 'NameText'},
              primaryImage: null,
              __typename: 'Name',
            },
            attributes: null,
            category: {id: 'actor', __typename: 'CreditCategory'},
            characters: null,
            episodeCredits: {
              total: 0,
              yearRange: null,
              __typename: 'EpisodeCastConnection',
            },
            __typename: 'Cast',
          },
          __typename: 'CreditEdge',
        },
        {
          node: {
            name: {
              id: 'nm0352031',
              nameText: {text: 'Charu Haasan', __typename: 'NameText'},
              primaryImage: {
                url: 'https://m.media-amazon.com/images/M/MV5BZDdjYmQ3MWItZmVmNC00M2UxLWJiODgtZDhlZjk2NzYzYmUxXkEyXkFqcGdeQXVyMzYxOTQ3MDg@._V1_.jpg',
                width: 2848,
                height: 4272,
                __typename: 'Image',
              },
              __typename: 'Name',
            },
            attributes: null,
            category: {id: 'actor', __typename: 'CreditCategory'},
            characters: [{name: 'Mr Rao', __typename: 'Character'}],
            episodeCredits: {
              total: 0,
              yearRange: null,
              __typename: 'EpisodeCastConnection',
            },
            __typename: 'Cast',
          },
          __typename: 'CreditEdge',
        },
        {
          node: {
            name: {
              id: 'nm0417301',
              nameText: {text: 'Janakaraj', __typename: 'NameText'},
              primaryImage: {
                url: 'https://m.media-amazon.com/images/M/MV5BMGQ4YmU0ODYtNjNmNS00Y2I1LTljOTktOGI5MjUwNzEwMTVjL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMjYwMDk5NjE@._V1_.jpg',
                width: 522,
                height: 684,
                __typename: 'Image',
              },
              __typename: 'Name',
            },
            attributes: [
              {text: 'as Janagaraj', __typename: 'CreditedAsCreditAttribute'},
            ],
            category: {id: 'actor', __typename: 'CreditCategory'},
            characters: null,
            episodeCredits: {
              total: 0,
              yearRange: null,
              __typename: 'EpisodeCastConnection',
            },
            __typename: 'Cast',
          },
          __typename: 'CreditEdge',
        },
        {
          node: {
            name: {
              id: 'nm0438092',
              nameText: {text: 'Dimple Kapadia', __typename: 'NameText'},
              primaryImage: {
                url: 'https://m.media-amazon.com/images/M/MV5BNTc4MTE1NDU5MF5BMl5BanBnXkFtZTcwNDI2MTcxOA@@._V1_.jpg',
                width: 2048,
                height: 1419,
                __typename: 'Image',
              },
              __typename: 'Name',
            },
            attributes: null,
            category: {id: 'actress', __typename: 'CreditCategory'},
            characters: null,
            episodeCredits: {
              total: 0,
              yearRange: null,
              __typename: 'EpisodeCastConnection',
            },
            __typename: 'Cast',
          },
          __typename: 'CreditEdge',
        },
        {
          node: {
            name: {
              id: 'nm0451166',
              nameText: {text: 'Amjad Khan', __typename: 'NameText'},
              primaryImage: {
                url: 'https://m.media-amazon.com/images/M/MV5BOWUzNjU3YTUtOGIyMi00MDk3LTk1NDMtMzY4MDZlNDFhNTFhXkEyXkFqcGdeQXVyMTExNDQ2MTI@._V1_.jpg',
                width: 670,
                height: 489,
                __typename: 'Image',
              },
              __typename: 'Name',
            },
            attributes: null,
            category: {id: 'actor', __typename: 'CreditCategory'},
            characters: null,
            episodeCredits: {
              total: 0,
              yearRange: null,
              __typename: 'EpisodeCastConnection',
            },
            __typename: 'Cast',
          },
          __typename: 'CreditEdge',
        },
        {
          node: {
            name: {
              id: 'nm0515488',
              nameText: {text: 'Lizy', __typename: 'NameText'},
              primaryImage: {
                url: 'https://m.media-amazon.com/images/M/MV5BZTA4NzE5MDEtYWExYS00MGMyLTkxM2ItZTZjY2UxZjFiYjZjXkEyXkFqcGdeQXVyMjkxNzQ1NDI@._V1_.jpg',
                width: 360,
                height: 533,
                __typename: 'Image',
              },
              __typename: 'Name',
            },
            attributes: [
              {text: 'as Lissy', __typename: 'CreditedAsCreditAttribute'},
            ],
            category: {id: 'actress', __typename: 'CreditCategory'},
            characters: [{name: 'Preethi', __typename: 'Character'}],
            episodeCredits: {
              total: 0,
              yearRange: null,
              __typename: 'EpisodeCastConnection',
            },
            __typename: 'Cast',
          },
          __typename: 'CreditEdge',
        },
        {
          node: {
            name: {
              id: 'nm7390393',
              nameText: {text: 'Aachi Manorama', __typename: 'NameText'},
              primaryImage: {
                url: 'https://m.media-amazon.com/images/M/MV5BMjJhMzRjYzYtZmJiNC00M2YzLTlhM2QtZTQxODIxNTk4MDBjXkEyXkFqcGdeQXVyMjYwMDk5NjE@._V1_.jpg',
                width: 780,
                height: 1112,
                __typename: 'Image',
              },
              __typename: 'Name',
            },
            attributes: [
              {text: 'as Manorama', __typename: 'CreditedAsCreditAttribute'},
            ],
            category: {id: 'actress', __typename: 'CreditCategory'},
            characters: [
              {name: 'Pallathhor RamaDevi', __typename: 'Character'},
            ],
            episodeCredits: {
              total: 0,
              yearRange: null,
              __typename: 'EpisodeCastConnection',
            },
            __typename: 'Cast',
          },
          __typename: 'CreditEdge',
        },
        {
          node: {
            name: {
              id: 'nm0151531',
              nameText: {text: 'Prathapachandran', __typename: 'NameText'},
              primaryImage: {
                url: 'https://m.media-amazon.com/images/M/MV5BM2MzZjUwYzItZjdhYy00MWJmLTllODAtMGY0YTBkNDIwZDk1XkEyXkFqcGdeQXVyMjkxNzQ1NDI@._V1_.jpg',
                width: 360,
                height: 533,
                __typename: 'Image',
              },
              __typename: 'Name',
            },
            attributes: null,
            category: {id: 'actor', __typename: 'CreditCategory'},
            characters: null,
            episodeCredits: {
              total: 0,
              yearRange: null,
              __typename: 'EpisodeCastConnection',
            },
            __typename: 'Cast',
          },
          __typename: 'CreditEdge',
        },
        {
          node: {
            name: {
              id: 'nm0708032',
              nameText: {text: 'V.K. Ramasamy', __typename: 'NameText'},
              primaryImage: null,
              __typename: 'Name',
            },
            attributes: null,
            category: {id: 'actor', __typename: 'CreditCategory'},
            characters: [{name: 'Minister', __typename: 'Character'}],
            episodeCredits: {
              total: 0,
              yearRange: null,
              __typename: 'EpisodeCastConnection',
            },
            __typename: 'Cast',
          },
          __typename: 'CreditEdge',
        },
        {
          node: {
            name: {
              id: 'nm0794478',
              nameText: {text: 'R.S. Shivaji', __typename: 'NameText'},
              primaryImage: {
                url: 'https://m.media-amazon.com/images/M/MV5BZWIyNGEyNjItN2EzMS00ZmIyLWEyYzktMjM0MTg0ODE4ZTc1XkEyXkFqcGdeQXVyMzYxOTQ3MDg@._V1_.jpg',
                width: 780,
                height: 1170,
                __typename: 'Image',
              },
              __typename: 'Name',
            },
            attributes: null,
            category: {id: 'actor', __typename: 'CreditCategory'},
            characters: [{name: 'Terrorist#1', __typename: 'Character'}],
            episodeCredits: {
              total: 0,
              yearRange: null,
              __typename: 'EpisodeCastConnection',
            },
            __typename: 'Cast',
          },
          __typename: 'CreditEdge',
        },
        {
          node: {
            name: {
              id: 'nm1176794',
              nameText: {text: 'Vani', __typename: 'NameText'},
              primaryImage: null,
              __typename: 'Name',
            },
            attributes: null,
            category: {id: 'actress', __typename: 'CreditCategory'},
            characters: null,
            episodeCredits: {
              total: 0,
              yearRange: null,
              __typename: 'EpisodeCastConnection',
            },
            __typename: 'Cast',
          },
          __typename: 'CreditEdge',
        },
      ],
      __typename: 'CreditConnection',
    },
    creators: [],
    directors: [
      {
        totalCredits: 1,
        category: {text: 'Director', __typename: 'CreditCategory'},
        credits: [
          {
            name: {
              id: 'nm2158003',
              nameText: {text: 'Rajasekar', __typename: 'NameText'},
              __typename: 'Name',
            },
            attributes: null,
            __typename: 'Crew',
          },
        ],
        __typename: 'PrincipalCreditsForCategory',
      },
    ],
    writers: [
      {
        totalCredits: 2,
        category: {text: 'Writers', __typename: 'CreditCategory'},
        credits: [
          {
            name: {
              id: 'nm0352032',
              nameText: {text: 'Kamal Haasan', __typename: 'NameText'},
              __typename: 'Name',
            },
            attributes: null,
            __typename: 'Crew',
          },
          {
            name: {
              id: 'nm0837675',
              nameText: {text: 'Sujatha', __typename: 'NameText'},
              __typename: 'Name',
            },
            attributes: null,
            __typename: 'Crew',
          },
        ],
        __typename: 'PrincipalCreditsForCategory',
      },
    ],
    isAdult: false,
    moreLikeThisTitles: {
      edges: [
        {
          node: {
            id: 'tt0320421',
            titleText: {text: 'Sathyaa', __typename: 'TitleText'},
            titleType: {
              id: 'movie',
              text: 'Movie',
              canHaveEpisodes: false,
              displayableProperty: {
                value: {plainText: '', __typename: 'Markdown'},
                __typename: 'DisplayableTitleTypeProperty',
              },
              __typename: 'TitleType',
            },
            originalTitleText: {text: 'Sathyaa', __typename: 'TitleText'},
            primaryImage: {
              id: 'rm1876919553',
              width: 1536,
              height: 2048,
              url: 'https://m.media-amazon.com/images/M/MV5BOTEzN2RmOWMtMTRmMS00Y2ExLWEwNGItMGIwMzgwZGU3NzgyXkEyXkFqcGdeQXVyODEzOTQwNTY@._V1_.jpg',
              caption: {
                plainText:
                  'Amala Akkineni, Bahadur, and Kamal Haasan in Sathyaa (1988)',
                __typename: 'Markdown',
              },
              __typename: 'Image',
            },
            releaseYear: {year: 1988, endYear: null, __typename: 'YearRange'},
            ratingsSummary: {
              aggregateRating: 7.9,
              voteCount: 1028,
              __typename: 'RatingsSummary',
            },
            runtime: {seconds: 8820, __typename: 'Runtime'},
            certificate: {rating: 'U', __typename: 'Certificate'},
            canRate: {isRatable: true, __typename: 'CanRate'},
            titleGenres: {
              genres: [
                {
                  genre: {text: 'Action', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
                {
                  genre: {text: 'Crime', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
                {
                  genre: {text: 'Drama', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
              ],
              __typename: 'TitleGenres',
            },
            canHaveEpisodes: false,
            __typename: 'Title',
          },
          __typename: 'MoreLikeThisEdge',
        },
        {
          node: {
            id: 'tt0140412',
            titleText: {text: 'Nammavar', __typename: 'TitleText'},
            titleType: {
              id: 'movie',
              text: 'Movie',
              canHaveEpisodes: false,
              displayableProperty: {
                value: {plainText: '', __typename: 'Markdown'},
                __typename: 'DisplayableTitleTypeProperty',
              },
              __typename: 'TitleType',
            },
            originalTitleText: {text: 'Nammavar', __typename: 'TitleText'},
            primaryImage: {
              id: 'rm1792380417',
              width: 1579,
              height: 2048,
              url: 'https://m.media-amazon.com/images/M/MV5BZWZlNjRmMzgtZTE1Ny00OGZhLTg1OWMtZWM3MTBjOWE5YzJiXkEyXkFqcGdeQXVyODEzOTQwNTY@._V1_.jpg',
              caption: {
                plainText:
                  'Gautami, Kamal Haasan, Nagesh, and Karan in Nammavar (1994)',
                __typename: 'Markdown',
              },
              __typename: 'Image',
            },
            releaseYear: {year: 1994, endYear: null, __typename: 'YearRange'},
            ratingsSummary: {
              aggregateRating: 7.7,
              voteCount: 885,
              __typename: 'RatingsSummary',
            },
            runtime: {seconds: 10680, __typename: 'Runtime'},
            certificate: {rating: 'U', __typename: 'Certificate'},
            canRate: {isRatable: true, __typename: 'CanRate'},
            titleGenres: {
              genres: [
                {
                  genre: {text: 'Drama', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
              ],
              __typename: 'TitleGenres',
            },
            canHaveEpisodes: false,
            __typename: 'Title',
          },
          __typename: 'MoreLikeThisEdge',
        },
        {
          node: {
            id: 'tt2199711',
            titleText: {text: 'Vishwaroopam', __typename: 'TitleText'},
            titleType: {
              id: 'movie',
              text: 'Movie',
              canHaveEpisodes: false,
              displayableProperty: {
                value: {plainText: '', __typename: 'Markdown'},
                __typename: 'DisplayableTitleTypeProperty',
              },
              __typename: 'TitleType',
            },
            originalTitleText: {text: 'Vishwaroopam', __typename: 'TitleText'},
            primaryImage: {
              id: 'rm3827736065',
              width: 1000,
              height: 1667,
              url: 'https://m.media-amazon.com/images/M/MV5BZGJhNmFmZTUtNGRkNC00ZWQ4LTllMDAtNGY4ZTE4NjQ3ZTI5XkEyXkFqcGdeQXVyODEzOTQwNTY@._V1_.jpg',
              caption: {
                plainText: 'Kamal Haasan in Vishwaroopam (2013)',
                __typename: 'Markdown',
              },
              __typename: 'Image',
            },
            releaseYear: {year: 2013, endYear: null, __typename: 'YearRange'},
            ratingsSummary: {
              aggregateRating: 7.9,
              voteCount: 40901,
              __typename: 'RatingsSummary',
            },
            runtime: {seconds: 8880, __typename: 'Runtime'},
            certificate: {rating: 'UA', __typename: 'Certificate'},
            canRate: {isRatable: true, __typename: 'CanRate'},
            titleGenres: {
              genres: [
                {
                  genre: {text: 'Action', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
                {
                  genre: {text: 'Thriller', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
              ],
              __typename: 'TitleGenres',
            },
            canHaveEpisodes: false,
            __typename: 'Title',
          },
          __typename: 'MoreLikeThisEdge',
        },
        {
          node: {
            id: 'tt0294264',
            titleText: {text: 'Aalavandhan', __typename: 'TitleText'},
            titleType: {
              id: 'movie',
              text: 'Movie',
              canHaveEpisodes: false,
              displayableProperty: {
                value: {plainText: '', __typename: 'Markdown'},
                __typename: 'DisplayableTitleTypeProperty',
              },
              __typename: 'TitleType',
            },
            originalTitleText: {text: 'Aalavandhan', __typename: 'TitleText'},
            primaryImage: {
              id: 'rm2437713665',
              width: 1629,
              height: 2048,
              url: 'https://m.media-amazon.com/images/M/MV5BN2YzZTVlYjItYzMxNS00NjljLWI3NjUtZDM4YmNlN2RkYWQwXkEyXkFqcGdeQXVyODEzOTQwNTY@._V1_.jpg',
              caption: {
                plainText: 'Kamal Haasan in Aalavandhan (2001)',
                __typename: 'Markdown',
              },
              __typename: 'Image',
            },
            releaseYear: {year: 2001, endYear: null, __typename: 'YearRange'},
            ratingsSummary: {
              aggregateRating: 7.2,
              voteCount: 2947,
              __typename: 'RatingsSummary',
            },
            runtime: {seconds: 10620, __typename: 'Runtime'},
            certificate: {rating: 'A', __typename: 'Certificate'},
            canRate: {isRatable: true, __typename: 'CanRate'},
            titleGenres: {
              genres: [
                {
                  genre: {text: 'Action', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
                {
                  genre: {text: 'Crime', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
                {
                  genre: {text: 'Thriller', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
              ],
              __typename: 'TitleGenres',
            },
            canHaveEpisodes: false,
            __typename: 'Title',
          },
          __typename: 'MoreLikeThisEdge',
        },
        {
          node: {
            id: 'tt0285665',
            titleText: {text: 'Kuruthipunal', __typename: 'TitleText'},
            titleType: {
              id: 'movie',
              text: 'Movie',
              canHaveEpisodes: false,
              displayableProperty: {
                value: {plainText: '', __typename: 'Markdown'},
                __typename: 'DisplayableTitleTypeProperty',
              },
              __typename: 'TitleType',
            },
            originalTitleText: {text: 'Kuruthipunal', __typename: 'TitleText'},
            primaryImage: {
              id: 'rm2545050880',
              width: 264,
              height: 384,
              url: 'https://m.media-amazon.com/images/M/MV5BMDUwNGRlOTQtYzc1ZS00YTMyLTk2MmUtNDE1NjlkMGU3YmE0XkEyXkFqcGdeQXVyODEzOTQwNTY@._V1_.jpg',
              caption: {
                plainText:
                  'Arjun Sarja and Kamal Haasan in Kuruthipunal (1995)',
                __typename: 'Markdown',
              },
              __typename: 'Image',
            },
            releaseYear: {year: 1995, endYear: null, __typename: 'YearRange'},
            ratingsSummary: {
              aggregateRating: 8.5,
              voteCount: 4661,
              __typename: 'RatingsSummary',
            },
            runtime: {seconds: 8580, __typename: 'Runtime'},
            certificate: {rating: 'A', __typename: 'Certificate'},
            canRate: {isRatable: true, __typename: 'CanRate'},
            titleGenres: {
              genres: [
                {
                  genre: {text: 'Action', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
                {
                  genre: {text: 'Drama', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
                {
                  genre: {text: 'Thriller', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
              ],
              __typename: 'TitleGenres',
            },
            canHaveEpisodes: false,
            __typename: 'Title',
          },
          __typename: 'MoreLikeThisEdge',
        },
        {
          node: {
            id: 'tt1417299',
            titleText: {text: 'Unnaipol Oruvan', __typename: 'TitleText'},
            titleType: {
              id: 'movie',
              text: 'Movie',
              canHaveEpisodes: false,
              displayableProperty: {
                value: {plainText: '', __typename: 'Markdown'},
                __typename: 'DisplayableTitleTypeProperty',
              },
              __typename: 'TitleType',
            },
            originalTitleText: {
              text: 'Unnaipol Oruvan',
              __typename: 'TitleText',
            },
            primaryImage: {
              id: 'rm1486326016',
              width: 600,
              height: 800,
              url: 'https://m.media-amazon.com/images/M/MV5BMTM0MjI5NDIyOF5BMl5BanBnXkFtZTcwNzc4NTI4Mg@@._V1_.jpg',
              caption: {
                plainText:
                  'Kamal Haasan and Mohanlal in Unnaipol Oruvan (2009)',
                __typename: 'Markdown',
              },
              __typename: 'Image',
            },
            releaseYear: {year: 2009, endYear: null, __typename: 'YearRange'},
            ratingsSummary: {
              aggregateRating: 8,
              voteCount: 6131,
              __typename: 'RatingsSummary',
            },
            runtime: {seconds: 6120, __typename: 'Runtime'},
            certificate: {rating: 'UA', __typename: 'Certificate'},
            canRate: {isRatable: true, __typename: 'CanRate'},
            titleGenres: {
              genres: [
                {
                  genre: {text: 'Action', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
                {
                  genre: {text: 'Crime', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
                {
                  genre: {text: 'Drama', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
              ],
              __typename: 'TitleGenres',
            },
            canHaveEpisodes: false,
            __typename: 'Title',
          },
          __typename: 'MoreLikeThisEdge',
        },
        {
          node: {
            id: 'tt0096827',
            titleText: {text: 'Apoorva Sagodharargal', __typename: 'TitleText'},
            titleType: {
              id: 'movie',
              text: 'Movie',
              canHaveEpisodes: false,
              displayableProperty: {
                value: {plainText: '', __typename: 'Markdown'},
                __typename: 'DisplayableTitleTypeProperty',
              },
              __typename: 'TitleType',
            },
            originalTitleText: {
              text: 'Apoorva Sagodharargal',
              __typename: 'TitleText',
            },
            primaryImage: {
              id: 'rm4097225473',
              width: 1552,
              height: 2048,
              url: 'https://m.media-amazon.com/images/M/MV5BZjkxNjg0MzItZTYxOS00NmY1LWE5ODUtNjU0ZmNjOWUzYTg2XkEyXkFqcGdeQXVyODEzOTQwNTY@._V1_.jpg',
              caption: {
                plainText:
                  'Gautami, Kamal Haasan, Nagesh, Komal Mahuvakar, and Srividya in Apoorva Sagodharargal (1989)',
                __typename: 'Markdown',
              },
              __typename: 'Image',
            },
            releaseYear: {year: 1989, endYear: null, __typename: 'YearRange'},
            ratingsSummary: {
              aggregateRating: 8.3,
              voteCount: 4539,
              __typename: 'RatingsSummary',
            },
            runtime: {seconds: 9420, __typename: 'Runtime'},
            certificate: {rating: 'U', __typename: 'Certificate'},
            canRate: {isRatable: true, __typename: 'CanRate'},
            titleGenres: {
              genres: [
                {
                  genre: {text: 'Action', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
                {
                  genre: {text: 'Comedy', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
                {
                  genre: {text: 'Drama', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
              ],
              __typename: 'TitleGenres',
            },
            canHaveEpisodes: false,
            __typename: 'Title',
          },
          __typename: 'MoreLikeThisEdge',
        },
        {
          node: {
            id: 'tt0140090',
            titleText: {text: 'Guna', __typename: 'TitleText'},
            titleType: {
              id: 'movie',
              text: 'Movie',
              canHaveEpisodes: false,
              displayableProperty: {
                value: {plainText: '', __typename: 'Markdown'},
                __typename: 'DisplayableTitleTypeProperty',
              },
              __typename: 'TitleType',
            },
            originalTitleText: {text: 'Guna', __typename: 'TitleText'},
            primaryImage: {
              id: 'rm1579977473',
              width: 1468,
              height: 2048,
              url: 'https://m.media-amazon.com/images/M/MV5BMzE4ZjQxNjMtOWVlYS00NzQwLTkyYjItNDM1OWNhZjlkNzg0XkEyXkFqcGdeQXVyODEzOTQwNTY@._V1_.jpg',
              caption: {
                plainText: 'Kamal Haasan in Guna (1991)',
                __typename: 'Markdown',
              },
              __typename: 'Image',
            },
            releaseYear: {year: 1991, endYear: null, __typename: 'YearRange'},
            ratingsSummary: {
              aggregateRating: 8.2,
              voteCount: 3054,
              __typename: 'RatingsSummary',
            },
            runtime: {seconds: 10020, __typename: 'Runtime'},
            certificate: {rating: 'A', __typename: 'Certificate'},
            canRate: {isRatable: true, __typename: 'CanRate'},
            titleGenres: {
              genres: [
                {
                  genre: {text: 'Drama', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
                {
                  genre: {text: 'Romance', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
              ],
              __typename: 'TitleGenres',
            },
            canHaveEpisodes: false,
            __typename: 'Title',
          },
          __typename: 'MoreLikeThisEdge',
        },
        {
          node: {
            id: 'tt0140534',
            titleText: {text: 'Sathi Leelavathi', __typename: 'TitleText'},
            titleType: {
              id: 'movie',
              text: 'Movie',
              canHaveEpisodes: false,
              displayableProperty: {
                value: {plainText: '', __typename: 'Markdown'},
                __typename: 'DisplayableTitleTypeProperty',
              },
              __typename: 'TitleType',
            },
            originalTitleText: {
              text: 'Sathi Leelavathi',
              __typename: 'TitleText',
            },
            primaryImage: {
              id: 'rm1216907777',
              width: 1489,
              height: 2048,
              url: 'https://m.media-amazon.com/images/M/MV5BN2YzNDJhYzktZTFjMy00YWQ3LTgxZGItZWYwMDVmMDhmZjliXkEyXkFqcGdeQXVyODEzOTQwNTY@._V1_.jpg',
              caption: {
                plainText:
                  'Ramesh Aravind, Kamal Haasan, and Heera Rajgopal in Sathi Leelavathi (1995)',
                __typename: 'Markdown',
              },
              __typename: 'Image',
            },
            releaseYear: {year: 1995, endYear: null, __typename: 'YearRange'},
            ratingsSummary: {
              aggregateRating: 8.1,
              voteCount: 1566,
              __typename: 'RatingsSummary',
            },
            runtime: {seconds: 9060, __typename: 'Runtime'},
            certificate: {rating: 'U', __typename: 'Certificate'},
            canRate: {isRatable: true, __typename: 'CanRate'},
            titleGenres: {
              genres: [
                {
                  genre: {text: 'Comedy', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
                {
                  genre: {text: 'Drama', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
              ],
              __typename: 'TitleGenres',
            },
            canHaveEpisodes: false,
            __typename: 'Title',
          },
          __typename: 'MoreLikeThisEdge',
        },
        {
          node: {
            id: 'tt9179430',
            titleText: {text: 'Vikram', __typename: 'TitleText'},
            titleType: {
              id: 'movie',
              text: 'Movie',
              canHaveEpisodes: false,
              displayableProperty: {
                value: {plainText: '', __typename: 'Markdown'},
                __typename: 'DisplayableTitleTypeProperty',
              },
              __typename: 'TitleType',
            },
            originalTitleText: {text: 'Vikram', __typename: 'TitleText'},
            primaryImage: {
              id: 'rm3408729345',
              width: 1365,
              height: 2048,
              url: 'https://m.media-amazon.com/images/M/MV5BZjE2NWU2MDAtNWM4OS00YjljLWJkYTEtMTFlZDg2NGUwMDYyXkEyXkFqcGdeQXVyMTM1OTU1MzQx._V1_.jpg',
              caption: {
                plainText:
                  'Kamal Haasan, Fahadh Faasil, and Vijay Sethupathi in Vikram (2022)',
                __typename: 'Markdown',
              },
              __typename: 'Image',
            },
            releaseYear: {year: 2022, endYear: null, __typename: 'YearRange'},
            ratingsSummary: {
              aggregateRating: 8.3,
              voteCount: 73690,
              __typename: 'RatingsSummary',
            },
            runtime: {seconds: 10500, __typename: 'Runtime'},
            certificate: {rating: 'UA', __typename: 'Certificate'},
            canRate: {isRatable: true, __typename: 'CanRate'},
            titleGenres: {
              genres: [
                {
                  genre: {text: 'Action', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
                {
                  genre: {text: 'Crime', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
                {
                  genre: {text: 'Drama', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
              ],
              __typename: 'TitleGenres',
            },
            canHaveEpisodes: false,
            __typename: 'Title',
          },
          __typename: 'MoreLikeThisEdge',
        },
        {
          node: {
            id: 'tt0140348',
            titleText: {text: 'Mahanadhi', __typename: 'TitleText'},
            titleType: {
              id: 'movie',
              text: 'Movie',
              canHaveEpisodes: false,
              displayableProperty: {
                value: {plainText: '', __typename: 'Markdown'},
                __typename: 'DisplayableTitleTypeProperty',
              },
              __typename: 'TitleType',
            },
            originalTitleText: {text: 'Mahanadhi', __typename: 'TitleText'},
            primaryImage: {
              id: 'rm1878695425',
              width: 384,
              height: 576,
              url: 'https://m.media-amazon.com/images/M/MV5BMDc1MDE5ZmItYmZjZC00OGZmLWI2MDMtNDFkMWFiODJmYjRlXkEyXkFqcGdeQXVyODEzOTQwNTY@._V1_.jpg',
              caption: {
                plainText:
                  'Kamal Haasan, Cochin Hanifa, and Sukanya in Mahanadhi (1994)',
                __typename: 'Markdown',
              },
              __typename: 'Image',
            },
            releaseYear: {year: 1994, endYear: null, __typename: 'YearRange'},
            ratingsSummary: {
              aggregateRating: 8.6,
              voteCount: 4420,
              __typename: 'RatingsSummary',
            },
            runtime: {seconds: 9720, __typename: 'Runtime'},
            certificate: {rating: 'U', __typename: 'Certificate'},
            canRate: {isRatable: true, __typename: 'CanRate'},
            titleGenres: {
              genres: [
                {
                  genre: {text: 'Crime', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
                {
                  genre: {text: 'Drama', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
              ],
              __typename: 'TitleGenres',
            },
            canHaveEpisodes: false,
            __typename: 'Title',
          },
          __typename: 'MoreLikeThisEdge',
        },
        {
          node: {
            id: 'tt3681422',
            titleText: {text: 'Uttama Villain', __typename: 'TitleText'},
            titleType: {
              id: 'movie',
              text: 'Movie',
              canHaveEpisodes: false,
              displayableProperty: {
                value: {plainText: '', __typename: 'Markdown'},
                __typename: 'DisplayableTitleTypeProperty',
              },
              __typename: 'TitleType',
            },
            originalTitleText: {
              text: 'Uttama Villain',
              __typename: 'TitleText',
            },
            primaryImage: {
              id: 'rm280990721',
              width: 1299,
              height: 1772,
              url: 'https://m.media-amazon.com/images/M/MV5BOGY3MzI1NWMtZTVlOC00OTQ5LTljYTktZjU0MWFlN2ZmYWNiXkEyXkFqcGdeQXVyODEzOTQwNTY@._V1_.jpg',
              caption: {
                plainText: 'Kamal Haasan in Uttama Villain (2015)',
                __typename: 'Markdown',
              },
              __typename: 'Image',
            },
            releaseYear: {year: 2015, endYear: null, __typename: 'YearRange'},
            ratingsSummary: {
              aggregateRating: 7.3,
              voteCount: 3674,
              __typename: 'RatingsSummary',
            },
            runtime: {seconds: 10260, __typename: 'Runtime'},
            certificate: {rating: 'U', __typename: 'Certificate'},
            canRate: {isRatable: true, __typename: 'CanRate'},
            titleGenres: {
              genres: [
                {
                  genre: {text: 'Comedy', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
                {
                  genre: {text: 'Drama', __typename: 'GenreItem'},
                  __typename: 'TitleGenre',
                },
              ],
              __typename: 'TitleGenres',
            },
            canHaveEpisodes: false,
            __typename: 'Title',
          },
          __typename: 'MoreLikeThisEdge',
        },
      ],
      __typename: 'MoreLikeThisConnection',
    },
    triviaTotal: {total: 3, __typename: 'TriviaConnection'},
    trivia: {
      edges: [
        {
          node: {
            text: {
              plaidHtml:
                'Even though it was boasted as one crore production it was not a mega hit in Tamil Nadu, whereas it did very well in neighbouring state Kerala.',
              __typename: 'Markdown',
            },
            trademark: null,
            relatedNames: null,
            __typename: 'TitleTrivia',
          },
          __typename: 'TriviaEdge',
        },
      ],
      __typename: 'TriviaConnection',
    },
    goofsTotal: {total: 0, __typename: 'GoofConnection'},
    goofs: {edges: [], __typename: 'GoofConnection'},
    quotesTotal: {total: 0, __typename: 'TitleQuoteConnection'},
    quotes: {edges: [], __typename: 'TitleQuoteConnection'},
    crazyCredits: {edges: [], __typename: 'CrazyCreditConnection'},
    alternateVersions: {
      total: 0,
      edges: [],
      __typename: 'AlternateVersionConnection',
    },
    connections: {
      edges: [
        {
          node: {
            associatedTitle: {
              id: 'tt9179430',
              releaseYear: {year: 2022, __typename: 'YearRange'},
              titleText: {text: 'Vikram', __typename: 'TitleText'},
              originalTitleText: {text: 'Vikram', __typename: 'TitleText'},
              series: null,
              __typename: 'Title',
            },
            category: {text: 'Spin-off', __typename: 'TitleConnectionCategory'},
            __typename: 'TitleConnection',
          },
          __typename: 'TitleConnectionEdge',
        },
      ],
      __typename: 'TitleConnectionConnection',
    },
    soundtrack: {edges: [], __typename: 'SoundtrackConnection'},
    titleText: {text: 'Vikram', __typename: 'TitleText'},
    originalTitleText: {text: 'Vikram', __typename: 'TitleText'},
    releaseYear: {year: 1986, __typename: 'YearRange'},
    reviews: {total: 9, __typename: 'ReviewsConnection'},
    featuredReviews: {
      edges: [
        {
          node: {
            id: 'rw3087694',
            author: {
              nickName: 'sanjayppc',
              userId: 'ur47571953',
              __typename: 'UserProfile',
            },
            summary: {
              originalText: 'Sci-fi gone astray',
              __typename: 'ReviewSummary',
            },
            text: {
              originalText: {
                plaidHtml:
                  'Vikram was one of the ambitious film projects of kamal, who collaborated with writer Sujatha. This was the last film in which Satyaraj acted as villain.<br/><br/>He had started acting as hero then &amp; was reluctant. But kamal insisted that he act as the villain with the assurance that he would later produce a film with him as hero. That film was &quot;Kadamai Kanniyam kattupadu&quot;.<br/><br/>Vikram had a good cast, lovely locales, melodious songs, gorgeous heroines &amp; a smart &amp; handsome kamal as a cop, yet it failed at the box office.<br/><br/>The reason was that that the usual masala elements were missing. Also, after a riveting first half, the film went haywire in the 2nd half when the story moved to Salamia (imaginary country). Janakaraj&#39;s antics were annoying &amp; also the pace of the film slackened a bit.<br/><br/>The trailer was in the theatres for a few months before its release raising great expectations, specially amongst kamal fans.<br/><br/>Over the years, it has gone on to become a classic &amp; shown repeatedly on TV.',
                __typename: 'Markdown',
              },
              __typename: 'ReviewText',
            },
            authorRating: 7,
            submissionDate: '2014-09-17',
            helpfulness: {
              upVotes: 2,
              downVotes: 0,
              __typename: 'ReviewHelpfulness',
            },
            __typename: 'Review',
          },
          __typename: 'ReviewEdge',
        },
      ],
      __typename: 'ReviewsConnection',
    },
    canRate: {isRatable: true, __typename: 'CanRate'},
    iframeAddReviewLink: {
      url: 'https://contribute.imdb.com/review/tt0320736/add?bus=imdb&return_url=https%3A%2F%2Fwww.imdb.com%2Fclose_me&site=web',
      __typename: 'ContributionLink',
    },
    topQuestions: {
      total: 14,
      edges: [
        {
          node: {
            attributeId: 'run-time',
            question: {
              plainText: 'How long is Vikram?',
              __typename: 'Markdown',
            },
            __typename: 'AlexaQuestion',
          },
          __typename: 'AlexaQuestionEdge',
        },
      ],
      __typename: 'AlexaQuestionConnection',
    },
    faqs: {total: 0, edges: [], __typename: 'FaqConnection'},
    releaseDate: {
      day: 29,
      month: 5,
      year: 1986,
      country: {
        id: 'IN',
        text: 'India',
        __typename: 'LocalizedDisplayableCountry',
      },
      __typename: 'ReleaseDate',
    },
    countriesOfOrigin: {
      countries: [{id: 'IN', text: 'India', __typename: 'CountryOfOrigin'}],
      __typename: 'CountriesOfOrigin',
    },
    detailsExternalLinks: {
      edges: [],
      total: 0,
      __typename: 'ExternalLinkConnection',
    },
    spokenLanguages: {
      spokenLanguages: [
        {id: 'ta', text: 'Tamil', __typename: 'SpokenLanguage'},
      ],
      __typename: 'SpokenLanguages',
    },
    akas: {
      edges: [
        {
          node: {text: 'Agent Vikram', __typename: 'Aka'},
          __typename: 'AkaEdge',
        },
      ],
      __typename: 'AkaConnection',
    },
    filmingLocations: {
      edges: [],
      total: 0,
      __typename: 'FilmingLocationConnection',
    },
    production: {
      edges: [
        {
          node: {
            company: {
              id: 'co0026065',
              companyText: {
                text: 'Raajkamal Films International',
                __typename: 'CompanyText',
              },
              __typename: 'Company',
            },
            __typename: 'CompanyCredit',
          },
          __typename: 'CompanyCreditEdge',
        },
      ],
      __typename: 'CompanyCreditConnection',
    },
    companies: {total: 1, __typename: 'CompanyCreditConnection'},
    productionBudget: null,
    lifetimeGross: null,
    openingWeekendGross: null,
    worldwideGross: null,
    technicalSpecifications: {
      soundMixes: {items: [], __typename: 'SoundMixes'},
      aspectRatios: {items: [], __typename: 'AspectRatios'},
      colorations: {
        items: [
          {
            conceptId: 'color',
            text: 'Color',
            attributes: [],
            __typename: 'Coloration',
          },
        ],
        __typename: 'Colorations',
      },
      __typename: 'TechnicalSpecifications',
    },
    runtime: {seconds: 8160, __typename: 'Runtime'},
    series: null,
    canHaveEpisodes: false,
    contributionQuestions: {
      contributionLink: {
        url: 'https://contribute.imdb.com/answers',
        __typename: 'ContributionQuestionsLink',
      },
      edges: [
        {
          node: {
            entity: {
              primaryImage: {
                url: 'https://m.media-amazon.com/images/M/MV5BOWI0MjY4MjItOWYzZC00Y2I0LThiNjYtMWU1NjFhNWQxNDhkXkEyXkFqcGdeQXVyOTIzODUxMjk@._V1_.jpg',
                width: 1599,
                height: 2048,
                caption: {
                  plainText: 'Kamal Haasan in Vikram (1986)',
                  __typename: 'Markdown',
                },
                __typename: 'Image',
              },
              __typename: 'Title',
            },
            questionId: 'tt0320736.title_aka.CA.en-US',
            questionText: {
              plainText:
                'By what name was Vikram (1986) officially released in Canada in English?',
              __typename: 'Markdown',
            },
            contributionLink: {
              url: 'https://contribute.imdb.com/answers?pinnedQuestion=tt0320736.title_aka.CA.en-US',
              __typename: 'ContributionQuestionsLink',
            },
            __typename: 'Question',
          },
          __typename: 'QuestionEdge',
        },
      ],
      __typename: 'QuestionConnection',
    },
    __typename: 'Title',
  },
  fake: {
    '#TITLE': 'Vikram',
    '#YEAR': 1986,
    '#IMDB_ID': 'tt0320736',
    '#RANK': 53721,
    '#ACTORS': 'Kamal Haasan, Sathyaraj',
    '#AKA': 'Vikram (1986) ',
    '#IMDB_URL': 'https://imdb.com/title/tt0320736',
    '#IMDB_IV':
      'https://t.me/iv?url=https%3A%2F%2Fimdb.com%2Ftitle%2Ftt0320736&rhash=77ed0696a538f4',
    '#IMG_POSTER':
      'https://m.media-amazon.com/images/M/MV5BNzYwOWY1MzEtNTU2Yi00NGU5LTg0MzktMzk4NDViZDY4ZDI0XkEyXkFqcGdeQXVyMTEzNzg0Mjkx._V1_.jpg',
    photo_width: 671,
    photo_height: 926,
  },
  storyLine: {
    id: 'tt0320736',
    summaries: {edges: [], __typename: 'PlotConnection'},
    outlines: {
      edges: [
        {
          node: {
            plotText: {
              plaidHtml:
                'Vikram, a cop who is mourning the death of his wife, is assigned to retrieve a missile. He is aided by a highly educated engineer Preethi, who holds the knowledge to disarm the nuke, in his mission.',
              __typename: 'Markdown',
            },
            experimental_translatedPlotText: null,
            __typename: 'Plot',
          },
          __typename: 'PlotEdge',
        },
      ],
      __typename: 'PlotConnection',
    },
    synopses: {edges: [], __typename: 'PlotConnection'},
    storylineKeywords: {
      edges: [
        {
          node: {
            legacyId: 'murder-of-a-pregnant-woman',
            text: 'murder of a pregnant woman',
            __typename: 'TitleKeyword',
          },
          __typename: 'TitleKeywordEdge',
        },
        {
          node: {
            legacyId: 'murder',
            text: 'murder',
            __typename: 'TitleKeyword',
          },
          __typename: 'TitleKeywordEdge',
        },
        {
          node: {
            legacyId: 'pregnancy',
            text: 'pregnancy',
            __typename: 'TitleKeyword',
          },
          __typename: 'TitleKeywordEdge',
        },
        {
          node: {
            legacyId: 'one-word-title',
            text: 'one word title',
            __typename: 'TitleKeyword',
          },
          __typename: 'TitleKeywordEdge',
        },
        {
          node: {
            legacyId: 'princess',
            text: 'princess',
            __typename: 'TitleKeyword',
          },
          __typename: 'TitleKeywordEdge',
        },
      ],
      total: 17,
      __typename: 'TitleKeywordConnection',
    },
    taglines: {edges: [], total: 0, __typename: 'TaglineConnection'},
    genres: {
      genres: [
        {id: 'Action', text: 'Action', __typename: 'Genre'},
        {id: 'Adventure', text: 'Adventure', __typename: 'Genre'},
        {id: 'Sci-Fi', text: 'Sci-Fi', __typename: 'Genre'},
      ],
      __typename: 'Genres',
    },
    certificate: null,
    parentsGuide: {
      guideItems: {total: 0, __typename: 'ParentsGuideConnection'},
      __typename: 'ParentsGuide',
    },
    __typename: 'Title',
  },
};
export default MOVIE_DETAILS