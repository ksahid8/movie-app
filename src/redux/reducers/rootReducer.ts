import { combineReducers } from '@reduxjs/toolkit';
import moviesReducer from 'src/redux/reducers/movieReducer';
import store from 'src/redux/store';

const rootReducer = combineReducers({
  movies: moviesReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch; 
export default rootReducer;
