import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { MovieDetailResponse, RandomMovieItem } from 'src/MovieSDK/types';

interface MoviesState {
  randomMovies: RandomMovieItem[];
  searchResults: RandomMovieItem[];
  movieDetails:MovieDetailResponse;
}

const initialState: MoviesState = {
  randomMovies:[],
  searchResults:[],
  movieDetails:{}
};

const moviesSlice = createSlice({
  name: 'movies',
  initialState,
  reducers: {
    setRandomMovies(state, action: PayloadAction<RandomMovieItem[]>) {
      return {
        ...state,
        randomMovies: action.payload,
      };
    },
    setSearchResults(state, action: PayloadAction<RandomMovieItem[]>) {
      return {
        ...state,
        searchResults: action.payload,
      };
    },
    setMovieDetails(state, action: PayloadAction<MovieDetailResponse>) {
      return {
        ...state,
        movieDetails: action.payload,
      };
    }
  },
});

export const { setRandomMovies,setSearchResults,setMovieDetails } = moviesSlice.actions;

export default moviesSlice.reducer;
