import { AppDispatch } from 'src/redux/reducers/rootReducer';
import { setRandomMovies, setSearchResults, setMovieDetails } from 'src/redux/reducers/movieReducer';
import { movieSDK } from 'src/MovieSDK';
import { MovieDetailResponse, RandomMovieItem } from 'src/MovieSDK/types'; 
import { Alert } from 'react-native';

export const fetchRandomMovies = () => {
  return async (dispatch: AppDispatch) => {
    try {
      const randomMovies: RandomMovieItem[] = await movieSDK.fetchRandomMovies() as RandomMovieItem[];
      dispatch(setRandomMovies(randomMovies));
    } catch (error) {
      console.error('Error fetching random movies:', error);
      Alert.alert('Error fetching random movies')
    }
  };
};


export const searchMovies = (query: string) => {
  return async (dispatch: AppDispatch) => {
    try {
      const searchResults: RandomMovieItem[] = await movieSDK.searchMovies(query) as RandomMovieItem[];
      dispatch(setSearchResults(searchResults));
    } catch (error) {
      console.error('Error searching movies:', error);
      Alert.alert('Error searching movies')
    }
  };
};

export const fetchMovieDetails = (movieId: string) => {
  return async (dispatch: AppDispatch) => {
    try {
      const movieDetailsResponse = await movieSDK.fetchMovieDetails(movieId);
      const movieDetails: MovieDetailResponse = movieDetailsResponse !== null ? movieDetailsResponse : {};
      dispatch(setMovieDetails(movieDetails));
    } catch (error) {
      console.error('Error fetching movie details:', error);
      Alert.alert('Error fetching movie details')
    }
  };
};
