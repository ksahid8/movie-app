import {configureStore} from '@reduxjs/toolkit';
import rootReducer from 'src/redux/reducers/rootReducer';

const store = configureStore({
  reducer: rootReducer,
});

export default store;
