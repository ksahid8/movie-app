import React from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {DETAILS_HEIGHT_SKELETON} from 'src/utils/constants';

export type DetailSkeletonProps = {};

const DetailSkeleton: React.FC<DetailSkeletonProps> = ({}) => {
  return (
    <SkeletonPlaceholder>
      <SkeletonPlaceholder.Item flexDirection="column">
        <SkeletonPlaceholder.Item
          width={'100%'}
          height={DETAILS_HEIGHT_SKELETON}
        />
        <SkeletonPlaceholder.Item flexDirection="column" paddingHorizontal={15}>
          <SkeletonPlaceholder.Item
            width={'100%'}
            height={125}
            marginTop={10}
            borderRadius={10}
          />
          <SkeletonPlaceholder.Item flexDirection="row" marginTop={10} gap={10}>
            <SkeletonPlaceholder.Item
              width={'49%'}
              height={100}
              borderRadius={10}
            />
            <SkeletonPlaceholder.Item
              width={'49%'}
              height={100}
              borderRadius={10}
            />
          </SkeletonPlaceholder.Item>
          <SkeletonPlaceholder.Item flexDirection="row" marginTop={10} gap={10}>
            <SkeletonPlaceholder.Item
              width={'49%'}
              height={30}
              borderRadius={10}
            />
            <SkeletonPlaceholder.Item
              width={'49%'}
              height={30}
              borderRadius={10}
            />
          </SkeletonPlaceholder.Item>
          <SkeletonPlaceholder.Item flexDirection="row" marginTop={10} gap={10}>
            <SkeletonPlaceholder.Item
              width={'49%'}
              height={30}
              borderRadius={10}
            />
            <SkeletonPlaceholder.Item
              width={'49%'}
              height={30}
              borderRadius={10}
            />
          </SkeletonPlaceholder.Item>
          <SkeletonPlaceholder.Item
            width={'100%'}
            height={150}
            marginTop={10}
            borderRadius={10}
          />
        </SkeletonPlaceholder.Item>
      </SkeletonPlaceholder.Item>
    </SkeletonPlaceholder>
  );
};

export default DetailSkeleton;
