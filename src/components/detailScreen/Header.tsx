import React, {useState} from 'react';
import {ActivityIndicator, Platform, StyleSheet, Text, View} from 'react-native';
import {COLORS} from 'src/utils/colors';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import IonIcon from 'react-native-vector-icons/Ionicons';
import {formatVoteCount} from 'src/utils/constants';
import {MovieDetailResponse} from 'src/MovieSDK/types';
import FastImage from 'react-native-fast-image';

interface HeaderProps {
  movieDetails: MovieDetailResponse;
}

const Header: React.FC<HeaderProps> = ({movieDetails}) => {
  const {short, top} = movieDetails;

  const releaseYear = top?.releaseDate?.year;
  const runTime = top?.runtime?.displayableProperty?.value.plainText;
  const [headerContentHeight, setHeaderContentHeight] = useState(0);

  const [imageLoaded, setimageLoaded] = useState(false);

  const titleText = top?.originalTitleText.text;
  const genreArray = short?.genre;
  const ratingValue = short?.aggregateRating?.ratingValue ?? 0;
  const ratingCount = formatVoteCount(short?.aggregateRating?.ratingCount) ?? 0;
  const reviewsCount = top?.reviews?.total ?? 0;
  const contentRating = short?.contentRating || 'U';
  const directorText =
    short?.director && short.director.length > 0
      ? `Directed by ${short.director[0]?.name}`
      : '';

  const renderImageContent = () => {
    return (
      <View>
        <FastImage
          source={{uri: short?.image}}
          style={styles.posterImage}
          onLoad={() => {
            setimageLoaded(true);
          }}
        />
        {!imageLoaded && (
          <View
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              width: '100%',
              height: '100%',
              backgroundColor: COLORS.grey,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 5,
            }}>
            <ActivityIndicator size="small" color={COLORS.greyText} />
          </View>
        )}
      </View>
    );
  };

  return (
    <View style={styles.headerBackground}>
      <FastImage
        source={{uri: short?.image}}
        style={[styles.backdropImage, {height: headerContentHeight}]}
      />
      <View style={styles.overlay} />
      <View
        style={styles.headerContent}
        onLayout={event => {
          const {height} = event.nativeEvent.layout;
          setHeaderContentHeight(height + 75);
        }}>
        <View style={styles.runtimeContainer}>
          {renderImageContent()}
          {(releaseYear || runTime) && (
            <Text style={styles.runtimeText}>
              {releaseYear && runTime
                ? `${releaseYear} - ${runTime}`
                : releaseYear
                ? releaseYear
                : runTime}
            </Text>
          )}
        </View>
        <View style={styles.textInfo}>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>{titleText}</Text>
          </View>
          <View style={styles.genreContainer}>
            {genreArray?.map((genre, index) => (
              <View style={styles.genreTag} key={index}>
                <Text style={styles.genreTagText}>{genre}</Text>
              </View>
            ))}
          </View>
          <View style={styles.boxContainer}>
            <View style={styles.imdbContainer}>
              <View style={styles.imdbHeaderContainer}>
                <Text style={styles.imdbHeaderContainerText}>IMDb RATING</Text>
              </View>
              <View style={styles.imdbRatingContainer}>
                <View style={styles.imdbIconContainer}>
                  <FontAwesomeIcon
                    name="star"
                    size={22}
                    color={COLORS.yellow}
                  />
                </View>
                <View style={styles.imdbCountContainer}>
                  <View style={styles.imdbRatingRow}>
                    <Text style={[styles.imdbRatingText, styles.bold]}>
                      {ratingValue}
                    </Text>
                    <Text style={styles.imdbRatingText}> / 10</Text>
                  </View>
                  <Text style={styles.imdbVotesText}>{ratingCount}</Text>
                </View>
              </View>
            </View>
            <View style={styles.reviewContainer}>
              <View style={styles.reviewHeaderContainer}>
                <Text style={styles.reviewHeaderContainerText}>REVIEWS</Text>
              </View>
              <View style={styles.reviewCountContainer}>
                <View style={styles.reviewIconContainer}>
                  <IonIcon
                    name="chatbox-ellipses-outline"
                    size={22}
                    color={COLORS.whiteBg}
                  />
                </View>
                <View style={styles.reviewCountTextContainer}>
                  <Text style={styles.reviewCountText}>{reviewsCount}</Text>
                </View>
              </View>
            </View>
            <View style={styles.certificateContainer}>
              <Text style={styles.certificateText}>{contentRating}</Text>
            </View>
          </View>
          <View style={styles.directorContainer}>
            <Text style={styles.directorText}>{directorText}</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  headerBackground: {
    position: 'relative',
    alignItems: 'center',
  },
  backdropImage: {
    width: '100%',
    resizeMode: 'cover',
  },
  overlay: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0, 0, 0, 0.9)',
  },
  headerContent: {
    position: 'absolute',
    top: Platform.OS==="ios"?85:55,
    left: 0,
    right: 0,
    padding: 15,
    flexDirection: 'row',
    gap: 15,
  },
  posterImage: {
    width: 100,
    height: 120,
    resizeMode: 'cover',
    borderRadius: 10,
  },
  runtimeContainer: {
    alignItems: 'center',
  },
  runtimeText: {
    color: COLORS.whiteText,
    fontSize: 12,
    marginTop: 15,
  },
  textInfo: {},
  titleContainer: {
    maxWidth: '95%',
    flexWrap: 'wrap',
  },
  title: {
    fontSize: 22,
    fontWeight: 'bold',
    color: COLORS.whiteText,
    maxWidth: '90%',
  },
  genreContainer: {
    flexDirection: 'row',
    gap: 10,
    flexWrap: 'wrap',
    marginTop: 15,
    maxWidth: '95%',
  },
  genreTag: {
    backgroundColor: 'transparent',
    borderColor: COLORS.whiteBg,
    borderWidth: 1,
    borderRadius: 5,
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
  genreTagText: {
    color: COLORS.whiteText,
    fontSize: 10,
  },
  boxContainer: {
    marginTop: 20,
    flexDirection: 'row',
    gap: 20,
  },
  imdbContainer: {},
  imdbHeaderContainer: {},
  imdbHeaderContainerText: {
    color: COLORS.whiteText,
    fontWeight: '500',
    fontSize: 12,
  },
  imdbRatingContainer: {
    marginTop: 3,
    flexDirection: 'row',
    gap: 5,
  },
  imdbIconContainer: {},
  imdbCountContainer: {},
  imdbRatingRow: {
    flexDirection: 'row',
  },
  imdbRatingText: {
    color: COLORS.whiteText,
    fontSize: 14,
  },
  bold: {
    fontWeight: 'bold',
  },
  imdbVotesText: {
    color: COLORS.whiteBg,
    fontWeight: '300',
    fontSize: 12,
  },
  reviewContainer: {},
  reviewHeaderContainer: {},
  reviewHeaderContainerText: {
    color: COLORS.whiteText,
    fontWeight: '500',
    fontSize: 12,
  },
  reviewCountContainer: {
    marginTop: 3,
    flexDirection: 'row',
    gap: 5,
  },
  reviewIconContainer: {},
  reviewCountTextContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  reviewCountText: {
    color: COLORS.whiteText,
    fontWeight: 'bold',
    fontSize: 16,
  },
  certificateContainer: {
    borderWidth: 2,
    borderColor: COLORS.whiteBg,
    borderRadius: 10,
    paddingHorizontal: 5,
    alignItems: 'center',
    justifyContent: 'center',
    minWidth: 30,
    minHeight: 30,
  },
  certificateText: {
    color: COLORS.whiteText,
    fontWeight: '400',
    fontSize: 16,
  },
  directorContainer: {
    marginVertical: 10,
  },
  directorText: {
    color: COLORS.whiteText,
    fontWeight: '500',
    fontSize: 14,
  },
});

export default Header;
