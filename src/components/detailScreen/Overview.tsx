import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {COLORS} from 'src/utils/colors';
import {MovieDetailResponse} from 'src/MovieSDK/types';

interface OverviewProps {
  movieDetails: MovieDetailResponse;
}

interface Creator {
  '@type'?: string;
  url?: string;
  name?: string;
}

const Overview: React.FC<OverviewProps> = ({movieDetails}) => {
  
  const {top, short} = movieDetails;

  const writtersArray = short?.creator
    ?.map((person: Creator) => person?.name)
    .filter(name => name);

  const writtersString = writtersArray?.join(', ');

  const overviewText = top?.plot?.plotText?.plainText

  if(!overviewText){
    return 
  }

  return (
    <View style={styles.overviewContainer}>
      <View style={styles.overviewHeaderContainer}>
        <Text style={styles.overviewHeaderText}>Overview</Text>
      </View>
      <View style={styles.overviewInfoContainer}>
        <Text style={styles.overviewInfoText}>
          {overviewText}
        </Text>
        <Text
          style={[
            styles.overviewInfoText,
            styles.writerText,
          ]}>{`Written by ${writtersString}`}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  overviewContainer: {
    paddingHorizontal: 15,
    paddingTop: 30,
  },
  overviewHeaderContainer: {
    marginBottom: 10,
  },
  overviewHeaderText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: COLORS.blackText,
  },
  overviewInfoContainer: {},
  overviewInfoText: {
    fontSize: 14,
    color: COLORS.blackText,
  },
  writerText: {
    fontWeight: '600',
    marginTop: 10,
  },
});

export default Overview;
