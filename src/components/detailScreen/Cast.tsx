import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {COLORS} from 'src/utils/colors';
import {MovieDetailResponse} from 'src/MovieSDK/types';
import AvatarCard from '../shared/AvatarCard';

interface CastProps {
  movieDetails: MovieDetailResponse;
}

const Cast: React.FC<CastProps> = ({movieDetails}) => {
  
  const {short} = movieDetails;

  const castArray: string[] = short?.actor?.map(item => item.name ?? '') ?? [];

  if(!(castArray?.length>0)){
    return 
  }

  return (
    <View style={styles.castContainer}>
      <View style={styles.castHeaderContainer}>
        <Text style={styles.castHeaderText}>Cast</Text>
      </View>
      <View style={styles.castTagContainer}>
        <View style={styles.castTags}>
          {castArray.map((name, index) => (
            <AvatarCard key={index} name={name} />
          ))}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  castContainer: {
    paddingHorizontal: 15,
    paddingTop: 30,
  },
  castHeaderContainer: {
    marginBottom: 10,
  },
  castHeaderText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: COLORS.blackText,
  },
  castTagContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 5,
  },
  castTags: {
    flexDirection: 'row',
    gap: 10,
    flexWrap: 'wrap',
    alignItems: 'center',
    maxWidth: '95%',
  },
});

export default Cast;
