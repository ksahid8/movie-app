import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {COLORS} from 'src/utils/colors';
import {MovieDetailResponse} from 'src/MovieSDK/types';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import {formatDate} from 'src/utils/constants';

interface ReviewProps {
  movieDetails: MovieDetailResponse;
}

interface Creator {
  '@type'?: string;
  url?: string;
  name?: string;
}

const Review: React.FC<ReviewProps> = ({movieDetails}) => {

const ratingValue = movieDetails.short?.review?.reviewRating?.ratingValue ?? 0
const reviewTitle = movieDetails.short?.review?.name
const reviewDescription = movieDetails.short?.review?.reviewBody
const reviewData = formatDate(movieDetails.short?.review?.dateCreated as string)
const authorName = movieDetails.short?.review?.author?.name

if(!reviewTitle){
  return
}

  return (
    <View style={styles.reviewContainer}>
      <View style={styles.reviewHeaderContainer}>
        <Text style={styles.reviewHeaderText}>Featured Review</Text>
      </View>
      <View style={styles.reviewCard}>
        <View style={styles.ratingContainer}>
          <FontAwesomeIcon name="star" size={16} color={COLORS.yellow} />
          <Text style={styles.ratingText}>
            Rating :{' '}
            {ratingValue} / 10
          </Text>
        </View>
        <Text style={styles.reviewTitleText}>
          {reviewTitle}
        </Text>
        <Text style={styles.reviewDescText}>
          {reviewDescription}
        </Text>
        <View style={styles.footerContainer}>
          <Text style={styles.ratingDateText}>
            {reviewData}
          </Text>
          <Text style={styles.ratingAuthorText}>
            By {authorName}
          </Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  reviewContainer: {
    paddingHorizontal: 15,
    paddingTop: 30,
    paddingBottom: 50,
  },
  reviewHeaderContainer: {
    marginBottom: 10,
  },
  reviewHeaderText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: COLORS.blackText,
  },
  reviewCard: {},
  ratingContainer: {
    flexDirection: 'row',
    gap: 5,
  },
  ratingText: {
    fontSize: 14,
    color: COLORS.blackText,
    fontWeight: '600',
  },
  reviewTitleText: {
    fontSize: 14,
    color: COLORS.blackText,
    fontWeight: '600',
    marginVertical: 10,
  },
  reviewDescText: {
    fontSize: 14,
    color: COLORS.greyText,
  },
  footerContainer: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  ratingDateText: {
    fontSize: 14,
    fontStyle: 'italic',
    color: COLORS.greyText,
    fontWeight: '400',
  },
  ratingAuthorText: {
    fontSize: 14,
    color: COLORS.blue,
    fontWeight: '400',
    textTransform: 'capitalize',
  },
});

export default Review;
