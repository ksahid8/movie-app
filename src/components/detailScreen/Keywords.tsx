import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {COLORS} from 'src/utils/colors';
import {MovieDetailResponse} from 'src/MovieSDK/types';
import Tag from '../shared/Tag';

interface KeywordsProps {
  movieDetails: MovieDetailResponse;
}

const Keywords: React.FC<KeywordsProps> = ({movieDetails}) => {
  const {short} = movieDetails;

  const KeywordsArray: string[] = short?.keywords
    ? short.keywords.split(',')
    : [];

  if (!(KeywordsArray?.length > 0)) {
    return;
  }

  return (
    <View style={styles.keywordsContainer}>
      <View style={styles.keywordsHeaderContainer}>
        <Text style={styles.keywordsHeaderText}>Keywords</Text>
      </View>
      <View style={styles.keywordsTagContainer}>
        <View style={styles.keywordsTags}>
          {KeywordsArray.map((name, index) => (
            <Tag key={index} name={name} />
          ))}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  keywordsContainer: {
    paddingHorizontal: 15,
    paddingTop: 30,
  },
  keywordsHeaderContainer: {
    marginBottom: 10,
  },
  keywordsHeaderText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: COLORS.blackText,
  },
  keywordsTagContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 5,
  },
  keywordsTags: {
    flexDirection: 'row',
    gap: 5,
    flexWrap: 'wrap',
    alignItems: 'center',
    maxWidth: '95%',
  },
});

export default Keywords;
