import React from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

export type MovieCardSkeletonProps = {};

const MovieCardSkeleton: React.FC<MovieCardSkeletonProps> = ({}) => {
  return (
    <SkeletonPlaceholder>
      <SkeletonPlaceholder.Item
        width={'100%'}
        height={125}
        marginTop={10}
        borderRadius={10}
      />
    </SkeletonPlaceholder>
  );
};

export default MovieCardSkeleton;
