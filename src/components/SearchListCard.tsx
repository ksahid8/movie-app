import React, { useState } from 'react';
import {ActivityIndicator, Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {COLORS} from 'src/utils/colors';
import {RandomMovieItem} from 'src/MovieSDK/types';

interface SearchListCardProps {
  item: RandomMovieItem;
  onPress: () => void;
}

const SearchListCard: React.FC<SearchListCardProps> = ({item, onPress}) => {
  const {'#AKA': title, '#IMG_POSTER': image} = item;
  const [imageLoaded, setimageLoaded] = useState(false);

  const renderImageContent = () => {
    return (
      <View>
        <FastImage
          source={{uri: image}}
          style={styles.listImage}
          onLoad={() => {
            setimageLoaded(true);
          }}
        />
        {!imageLoaded && (
          <View
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              width: '100%',
              height: '100%',
              backgroundColor: COLORS.grey,
              alignItems:"center",
              justifyContent:"center",
              borderRadius:5
            }}>
            <ActivityIndicator size="small" color={COLORS.greyText} />
          </View>
        )}
      </View>
    );
  };

  return (
    <TouchableOpacity style={styles.suggestionItem} onPress={onPress}>
      {renderImageContent()}
      <Text numberOfLines={1} ellipsizeMode="tail" style={styles.listItemText}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  suggestionItem: {
    paddingVertical: 5,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.lightBg,
    flexDirection: 'row',
    gap: 15,
    alignItems: 'center',
  },
  listImage: {
    height: 40,
    width: 40,
    borderRadius: 5,
  },
  listItemText: {
    flex: 1,
    fontSize: 14,
    color: COLORS.blackText,
    overflow: 'hidden',
  },
});

export default SearchListCard;
