import React from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

export type SearchListCardSkeletonProps = {};

const SearchListCardSkeleton: React.FC<SearchListCardSkeletonProps> = ({}) => {
  return (
    <SkeletonPlaceholder>
      <SkeletonPlaceholder.Item
        width={'100%'}
        height={50}
        marginTop={10}
        borderRadius={10}
      />
    </SkeletonPlaceholder>
  );
};

export default SearchListCardSkeleton;
