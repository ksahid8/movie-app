import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {COLORS} from 'src/utils/colors';
interface TagProps {
  name: string;
}

const Tag: React.FC<TagProps> = ({name}) => {
  return (
    <View style={styles.tagItem}>
      <Text style={styles.tagItemText}>{name}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  tagItem: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 5,
    backgroundColor: COLORS.lightBg,
  },
 tagItemText: {
    fontSize: 12,
    fontWeight: '400',
    color: COLORS.blackText,
  },
});

export default Tag;
