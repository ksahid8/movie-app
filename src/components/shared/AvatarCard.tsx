import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {COLORS} from 'src/utils/colors';
interface AvatarCardProps {
  name: string;
}

const AvatarCard: React.FC<AvatarCardProps> = ({name}) => {
  return (
    <View style={styles.avatarItem}>
      <View style={styles.charAvatarContainer}>
        <Text style={styles.charAvatarText}>{name[0]}</Text>
      </View>
      <Text numberOfLines={1} style={styles.avatarItemText}>{name}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  avatarItem: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    borderRadius: 5,
    alignItems: "center",
    gap: 10,
    backgroundColor: COLORS.lightBg,
    width:"48%"
  },
  charAvatarContainer: {
    height: 40,
    width: 40,
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: COLORS.grey
  },
  charAvatarText: {
    fontSize: 18,
    fontWeight: '400',
    color: COLORS.blackText,
  },
  avatarItemText: {
    fontSize: 12,
    fontWeight: '400',
    color: COLORS.blackText,
    textAlign: 'center', 
  },
});


export default AvatarCard;
