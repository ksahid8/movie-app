import React, {useState} from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {COLORS} from 'src/utils/colors';
import {RandomMovieItem} from 'src/MovieSDK/types';
import Tag from './shared/Tag';
import IonIcon from 'react-native-vector-icons/Ionicons';
import FastImage from 'react-native-fast-image';

interface MovieCardProps {
  movie: RandomMovieItem;
  onPress: () => void;
}

const MovieCard: React.FC<MovieCardProps> = ({movie, onPress}) => {
  const {'#AKA': title, '#IMG_POSTER': image, '#ACTORS': actors} = movie;

  const castArray = actors.split(', ');

  const [imageLoaded, setimageLoaded] = useState(false);

  const renderImageContent = () => {
    return (
      <View>
        <FastImage
          source={{uri: image}}
          style={styles.image}
          onLoad={() => {
            setimageLoaded(true);
          }}
        />
        {!imageLoaded && (
          <View
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              width: '100%',
              height: '100%',
              backgroundColor: COLORS.grey,
              alignItems:"center",
              justifyContent:"center",
              borderRadius:5
            }}>
            <ActivityIndicator size="small" color={COLORS.greyText} />
          </View>
        )}
      </View>
    );
  };

  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.cardContainer}>
        <View style={styles.cardTextInfo}>
          <Text numberOfLines={1} ellipsizeMode="tail" style={styles.title}>
            {title}
          </Text>
          <View style={styles.castTagContainer}>
            <IonIcon
              name="film-outline"
              size={22}
              color={COLORS.greyText}
              style={styles.castIcon}
            />
            <View style={styles.castTags}>
              {castArray.map((name, index) => (
                <Tag key={index} name={name} />
              ))}
            </View>
          </View>
        </View>
        <View style={styles.imageContainer}>{renderImageContent()}</View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  cardContainer: {
    flexDirection: 'row',
    marginVertical: 5,
    borderRadius: 10,
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: COLORS.lightBg,
  },
  cardTextInfo: {
    flex: 1,
    flexDirection: 'column',
    gap: 10,
    padding: 15,
    backgroundColor: COLORS.whiteBg,
    justifyContent: 'center',
  },
  title: {
    fontWeight: '400',
    fontSize: 16,
    color: COLORS.blue,
  },
  castTagContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 5,
  },
  castTags: {
    flexDirection: 'row',
    gap: 5,
    flexWrap: 'wrap',
    alignItems: 'center',
    maxWidth: '95%',
  },
  castIcon: {},
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    height: 100,
    resizeMode: 'cover',
    aspectRatio: 14 / 18,
  },
});

export default MovieCard;
