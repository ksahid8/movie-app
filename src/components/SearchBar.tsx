import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {COLORS} from 'src/utils/colors';
import Icon from 'react-native-vector-icons/FontAwesome';

interface SearchBarProps {
  onPress: () => void;
}

const SearchBar: React.FC<SearchBarProps> = ({onPress}) => {
  return (
    <TouchableOpacity style={styles.searchBarContainer} onPress={onPress}>
      <View style={styles.searchBox}>
        <View style={styles.searchInnerBox}>
          <Text style={styles.placeholderText}>Search Movies..</Text>
          <Icon name="search" size={16} color={COLORS.greyText} />
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  searchBarContainer: {
    backgroundColor: COLORS.whiteBg,
    padding: 10,
    borderBottomColor: COLORS.lightBg,
    borderBottomWidth: 1,
  },
  searchBox: {},
  searchInnerBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderColor: COLORS.lightBg,
    borderRadius: 5,
    backgroundColor: COLORS.lightBg,
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  placeholderText: {
    fontSize: 14,
    color: COLORS.greyText,
  },
});

export default SearchBar;
