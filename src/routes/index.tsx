import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {RootStackParamList} from 'src/MovieSDK/types';
import HomeScreen from 'src/screens/HomeScreen';
import DetailScreen from 'src/screens/DetailScreen';
import SearchScreen from 'src/screens/SearchScreen';
import IonIcon from 'react-native-vector-icons/Ionicons';
import {COLORS} from 'src/utils/colors';
import {StyleSheet} from 'react-native';

const Stack = createStackNavigator<RootStackParamList>();

const Routes = () => {
  const GoBackArrow = ({color}: {color: string}) => (
    <IonIcon
      name="chevron-back-outline"
      size={24}
      color={color}
      style={styles.goBackIcon}
    />
  );

  const homeScreenOptions = {
    headerTitle: 'Movies',
    headerTitleStyle: {
      color: COLORS.blue,
    }
  };

  const searchScreenOptions = {
    headerTitle: 'Search',
    headerTitleStyle: {
      color: COLORS.blue,
    },
    headerBackImage: () => <GoBackArrow color={COLORS.blackText} />,
    headerBackTitle: ' ',
  };

  const detailScreenOptions = {
    headerTransparent: true,
    headerTitle: ' ',
    headerTitleStyle: {
      color: COLORS.whiteText,
    },
    headerBackImage: () => <GoBackArrow color={COLORS.whiteText} />,
    headerBackTitle: ' ',
  };

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{...homeScreenOptions,headerTitleAlign:"center"}}
        />
        <Stack.Screen
          name="Search"
          component={SearchScreen}
          options={{...searchScreenOptions,headerTitleAlign:"center"}}
        />
        <Stack.Screen
          name="Detail"
          component={DetailScreen}
          options={detailScreenOptions}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  goBackIcon: {
    paddingLeft: 5,
  },
});

export default Routes;
