```markdown
# Movie App

Welcome to my React Native movie app! This app allows you to browse and discover information about various movies.

## Prerequisites

Before getting started, ensure you have the following software installed on your system:

- Node.js (version >= 18)
- npm or Yarn
- Xcode (for iOS )
- Android Studio (for Android)

## Installation

After cloning the repository, navigate to the project directory and install the dependencies:

```bash
npm install
```

### iOS Setup

Navigate to the iOS directory and install the CocoaPods dependencies:

```bash
cd ios
pod install
```

### Android Setup

For Android development, no additional setup is required.

## Running the App

### iOS

To run the app on iOS simulator:

```bash
npm run ios
```

### Android

To run the app on an Android emulator or connected device:

```bash
npm run android
```

## MovieSDK

This app utilizes a custom Movie SDK to fetch movie data efficiently. The SDK is located in the src/sdk directory and provides functionalities to fetch random movies, search for movies, and retrieve movie details.


## Development

Feel free to explore the codebase. If you encounter any issues or have suggestions for improvements, please don't hesitate to open an issue or submit a pull request.

## Troubleshooting

### Issue with React Vector Icons

If you encounter an issue with React Vector Icons while running `npm run ios`, you can follow these steps to resolve it:

1. **Open Xcode**: Launch Xcode on your system.

2. **Copy Fonts Folder**: Navigate to the `node_modules` directory in your project and locate the `react-native-vector-icons` package. Copy the `Fonts` folder from `node_modules/react-native-vector-icons` to your project directory.

3. **Paste Fonts Folder into Project**: Paste the copied `Fonts` folder into your project directory.

4. **Install CocoaPods Dependencies**: Navigate to the `ios` directory of your project in the terminal and run the following command to install CocoaPods dependencies:

    ```bash
    cd ios
    pod install
    ```

5. **Run the App**: Once the CocoaPods dependencies are installed, you can run the app using the following command:

    ```bash
    npm run ios
    ```

For more detailed installation instructions, you can refer to the [React Native Vector Icons documentation](https://github.com/oblador/react-native-vector-icons?tab=readme-ov-file#installation).

If you continue to experience issues or have any questions, feel free to [contact me](mailto:ksahid8@gmail.com).


